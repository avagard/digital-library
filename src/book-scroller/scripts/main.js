(function (global, $) {
  'use strict';

  var
    _ = global._,
    currentBook,
    books = {};

  function loadBook() {

    var options,
      hash = global.location.hash.slice(1),
      bookId, bookInfo,
      doc, frame,
      w, h, p,
      initialPage = 1,
      indexId,
      hashArray = hash.split('_'),
      info;

    bookId = _.parseInt(hashArray[0]);

    indexId = _.parseInt(hashArray[2]);

    info = _.parseInt(hashArray[3]);

    if (_.isNaN(bookId)) {
      bookId = 0;
    }

    if (isNaN(indexId)) {
      indexId = -1;
    }

    if (hashArray.length > 1) {
      initialPage = Number(hashArray[1]);
      if (_.isNaN(initialPage)) {
        initialPage = 1;
      }
    }

    if (global.parent !== global && bookId) {

      options = _.booksOptions[bookId];

      if (info) {
        try {
          info = decodeURIComponent(info);
          info = JSON.parse(info);
        } catch (err) {
          console.warn('Invalid info args object is passed:', info);
          console.warn(err);
        }
        options.info = _.isObject(info) ? info : {};
      }

      if (initialPage) {
        options.initialPage = initialPage;
      }

      options.load = (function (bookId, indexId, info) {
        window._info = info;
        return function () {
          _.highlightIndex(indexId);
          _.loadPinpoints(bookId, options);
        };
      }(bookId, indexId, options));

      currentBook = bookId;

      _.currentBook = currentBook;

      doc = global.parent.document;

      frame = $(doc.querySelector('iframe'));

      w = frame.outerWidth() - 10;
      h = frame.outerHeight() - 10;
      $('.container').width(w);
      $('.container').height(h);

      $('#wrapper').width(w);

      _.expose('frameBox', {
        width: w,
        height: h
      });

      //fixPageSize(w, h);

    } else {

      $('body').addClass('test');

      options = {
        info: {},
        initialPage: initialPage,
        container: '.container',
        wrapper: '#postswrapper',
        loader: 'div#loadmoreajaxloader',
        count: 100,
        urlPattern: '/RTFHandler.ashx?userID=1&arg=52_{0}_4040&_dc=1410206563675',
        //urlPattern: function (pageNumber) {
        //  pageNumber = pageNumber < 10 ? '00' + pageNumber : (pageNumber < 100 ? '0' + pageNumber : String(pageNumber));
        //  return 'json/pg' + pageNumber + '.html';
        //},

        dataCallback: function (data) {
          return data.replace('text-align:Left;', '');
        }
      };
    }

    $.bookScroller.clear(options);

    $.bookScroller(options);
  }

  $(function () {
    $(window).on('load', function () {
      _.stopVideo();
    });
    loadBook();
  });

  //$(window).on('hashchange', loadBook);
}(this, jQuery));