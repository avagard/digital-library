(function (global, $) {
  'use strict';

  var _ = global._,
    highlighter,
    pinpointsCache = {},
    localStorage = (global.parent !== global) ? global.parent.localStorage : global.localStorage;

  _.expose('$', $);

  $(function () {
    //$('#postswrapper').textHighlighter();
    //204

    //highlighter = $('#postswrapper').getHighlighter();

    //expose to global
    //_.highlighter = highlighter;

    //_.highlighter.setColor('#fe57a1');

    // $('#postswrapper .page div').click(function () {
    //   _.highlighter.setColor('red');
    // });

  });

  //! \"#%&\'()*,-./:;?@[\\]_{|}¡¦§©«¬®°µ¶·»¿–—―‗‘’‚‛“”„†‡•…‰′″‹›‼‾⁄01234567890$¢£¤¥₣₤₧₪₫€¹²³℅ℓ№™Ω℮¼½¾⅓⅔⅛⅜⅝⅞+<=>±×÷∂∆∏∑−∕∙√∞∟∩∫≈≠≡≤≥،؛؟٠١٢٣٤٥٦٧٨٩٪٫٬٭۰۱۲۳۴۵۶۷۸۹\n\r\t\v\f
  var wordDelimiters = ['.', ' ', ',', '،', '!', '0-8', '۰۱۲۳۴۵۶۷۸۹', '*', '"', '“', '”', '¿', '#', '%', '&'].join('');

  function getTextNodesIn(node) {
    var textNodes = [],
      isParagraph = false,
      isEOF = false;

    if (node.nodeType === 3) {
      textNodes.push(node);
    } else {
      var children = node.childNodes;

      if (node.tagName.toLowerCase() === 'p') {
        isParagraph = true;
      }
      if (node.tagName.toLowerCase() === 'span' && node.innerHTML === '') {
        isEOF = true;
      }

      for (var i = 0, len = children.length; i < len; ++i) {
        textNodes.push.apply(textNodes, getTextNodesIn(children[i]));
      }
    }
    if (isParagraph || isEOF) {
      textNodes.push(document.createTextNode(' '));
    }
    return textNodes;
  }
  _.expose('getTextNodesIn', getTextNodesIn);

  function manipulateRange(el, start, end, doSelect, keepOld) {
    var range,
      textNodes,
      foundStart = false,
      charCount = 0,
      selection,
      textRange;

    if (document.createRange && window.getSelection) {
      range = document.createRange();

      range.selectNodeContents(el);
      textNodes = getTextNodesIn(el);

      _.some(textNodes, function (textNode, i) {
        var endCharCount = charCount + textNode.length;
        if (!foundStart &&
          start >= charCount &&
          (textNode.textContent.length > start - charCount) &&
          (start < endCharCount ||
            (start === endCharCount && i < textNodes.length))) {
          range.setStart(textNode, start - charCount);
          foundStart = true;
        }
        if (foundStart && end <= endCharCount) {
          range.setEnd(textNode, end - charCount);
          return true;
        }
        charCount = endCharCount;
      });

      // for (var i = 0, textNode; (textNode = textNodes[i++]);) {

      //   endCharCount = charCount + textNode.length;
      //   if (!foundStart &&
      //     start >= charCount &&
      //     (textNode.textContent.length > start - charCount) &&
      //     (start < endCharCount ||
      //       (start === endCharCount && i < textNodes.length))) {
      //     range.setStart(textNode, start - charCount);
      //     foundStart = true;
      //   }
      //   if (foundStart && end <= endCharCount) {
      //     range.setEnd(textNode, end - charCount);
      //     break;
      //   }
      //   charCount = endCharCount;

      // }
      if (doSelect) {
        selection = window.getSelection();
        if (!keepOld) {
          selection.removeAllRanges();
        }
        selection.addRange(range);
      }
      return range;
    } else if (document.selection && document.body.createTextRange) {
      textRange = document.body.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(true);
      textRange.moveEnd('character', end);
      textRange.moveStart('character', start);
      if (doSelect) {
        //TODO: check keepOld for previously selected ranges
        textRange.select();
      }
      return textRange;
    }
  }

  function highlightRange(el, start, end, keepOld) {
    return manipulateRange(el, start, end, true, keepOld);
  }
  _.expose('highlightRange', highlightRange);

  function findRange(pageNumber, start, end) {
    if (!_.isNumber(pageNumber) || pageNumber <= 0) {
      console.warn('To call findRange(...) pageNumber(greater than 0) is mandatory!');
      return;
    }
    if (!_.isNumber(start) || start < 0) {
      console.warn('To call findRange(...) start(greater than 0) is mandatory!');
      return;
    }
    if (!end) {
      end = start + 1;
    }

    var $wrapper = $('.page[page=' + pageNumber + ']'),
      el = $wrapper.get(0),
      range;

    if (!el) {
      console.warn('Couldn\'t find the page!');
      return;
    }

    return manipulateRange(el, start, end);
  }
  _.expose('findRange', findRange);

  function findPosition(pageNumber, position) {
    var range = findRange(pageNumber, position);
    if (!range) {
      console.warn('Invalid input args to call findPosition(...)');
      return;
    }
    var
      baseNode = range.startContainer || range.baseNode,
      position = $(baseNode.parentNode).position();

    return {
      rect: range.getBoundingClientRect(),
      position: position,
      width: $(baseNode.parentNode).width()
    };
  }
  _.expose('findPosition', findPosition);

  function setPinpoint(pageNumber, position, pinpoint) {
    var pagePinpoints = pinpointsCache[pageNumber];
    if (!_.isObject(pagePinpoints)) {
      pinpointsCache[pageNumber] = pagePinpoints = {};
    }
    pagePinpoints[position] = pinpoint;
  }

  function getPinpoint(pageNumber, position) {
    var pagePinpoints = pinpointsCache[pageNumber];
    return _.isObject(pagePinpoints) && pagePinpoints[position];
  }

  function showPinpoint(pageNumber, position) {
    var
      pinpointInfo = getPinpoint(pageNumber, position),
      positionInfo,
      top,
      scrollTop;

    if (pinpointInfo) {
      pinpointInfo.dom.css('display', '');
      pinpointInfo.dom.css('visibility', '');
    }
    if (pinpointInfo) {
      var position = pinpointInfo.positionInfo;

    } else {
      positionInfo = findPosition(pageNumber, position);
      if (!positionInfo) {
        console.warn('[_.showPinpoint(' + pageNumber + ', ' + position + ')]: Invalid position info!');
        return;
      }

      top = positionInfo.position.top;

      pinpointInfo = {
        positionInfo: positionInfo
      };

      pinpointInfo.dom = $('<div>', {
        'class': 'pin'
      })
        .css('top', (top - 30) + 'px')
        .css('left', positionInfo.position.left + positionInfo.width - 20 + 'px')
        .appendTo('.page[page=' + pageNumber + ']');

      setPinpoint(pageNumber, position, pinpointInfo);

      //add it to page!!
    }
  }
  _.expose('showPinpoint', showPinpoint);

  function hidePin() {
    $('#pin').css('display', 'none');
  }
  _.expose('hidePin', hidePin);

  function loadPinpoints(bookId, options) {
    var pinpoints = _.pinpointsCache[bookId];

    _.forIn(pinpoints, function (positions, pageNumber) {
      pageNumber = _.parseInt(pageNumber);
      _.each(positions, function (position) {
        if (_.contains(options.pages, pageNumber)) {
          _, showPinpoint(pageNumber, position);
        }
      });
    });
  }
  _.expose('loadPinpoints', loadPinpoints);

  // function loadPagePinpoints(bookId, pageNumber) {
  //   var pinpoints = _.pinpointsCache[bookId],
  //     positions = pinpoints[pageNumber];
  //   _.each(positions, function (position) {
  //     _.showPinpoint(pageNumber, position);
  //   });
  // }
  // _.expose('loadPagePinpoints', loadPagePinpoints);

  _.expose('getSelectionBottom', function () {
    var sel,
      rect;
    if (window.getSelection) {
      sel = window.getSelection();
      rect = sel.focusNode.parentNode.getBoundingClientRect();
      return _.frameBox.height - rect.top;
    } else if (document.selection) {
      sel = document.selection.createRange();
      return _.frameBox.height - sel.offsetTop;
    } else {
      console.error('We dont suuport this browser!');
    }
  });

  _.expose('getSelectedRange', function () {
    var text = '',
      selectedRange,
      startNode,
      endNode;
    if (window.getSelection) {
      selectedRange = window.getSelection();
      text = selectedRange.toString();
      startNode = _.isElement(selectedRange.baseNode) ? selectedRange.baseNode : selectedRange.baseNode.parentNode;
      endNode = _.isElement(selectedRange.extentNode) ? selectedRange.extentNode : selectedRange.extentNode.parentNode;
    } else if (document.selection && document.selection.type !== 'Control') {
      selectedRange = document.selection.createRange();
      text = selectedRange.text;

      //TODO: This needs to be fixed for IE!
      console.warn('This needs to be fixed for IE!');
      startNode = null;
      endNode = null;
    }
    return {
      text: text,
      selectedRange: selectedRange,
      startNode: startNode,
      endNode: endNode
    };
  });

  _.expose('highlightIndex', function (indexId) {
    var indexInfo,
      i;
    if (indexId > -1 && (indexInfo = _.indexesInfo[indexId])) {
      // info = {
      //   bookId: startDocInfo.bookId,
      //   startingPage: startDocInfo.pageNumber,
      //   startPosition: data.StartPosition,
      //   endingPage: endDocInfo.pageNumber,
      //   endPosition: data.EndPosition
      // };
      if (indexInfo.startingPage === indexInfo.endingPage) {
        _.highlightPage(indexInfo.startingPage, indexInfo.startPosition, indexInfo.endPosition);
      } else {
        _.highlightPage(indexInfo.startingPage, indexInfo.startPosition);

        i = indexInfo.startingPage + 1;

        for (; i < indexInfo.endingPage; i += 1) {
          _.highlightPage(i, 0);
        }

        _.highlightPage(indexInfo.endingPage, 0, indexInfo.endPosition);
      }
    }
  });

  _.expose('highlightPage', function (pageNumber, start, end) {
    if (!_.isNumber(pageNumber) || pageNumber <= 0) {
      console.warn('To call highlightPage(...) pageNumber(greater than 0) is mandatory!');
      return;
    }
    var $wrapper = $('.page[page=' + pageNumber + ']'),
      el = $wrapper.get(0),
      range;

    if (!el) {
      console.warn('Couldn\'t find the page!');
      return;
    }

    if (!_.isNumber(start) || start < 0) {
      start = 0;
    }

    if (!_.isNumber(end) || end < 0) {
      end = $(el).text().length;
    }

    range = highlightRange(el, start, end, true);

    if (!range) {
      console.warn('Couldn\'t find the selected range!');
      return;
    }

    return range;
    //highlighter._currentRange = range;

    //highlighter.highlightRange(range, $wrapper);
  });

  _.expose('getPageElement', function (pageNumber) {
    if (!_.isNumber(pageNumber) || pageNumber <= 0) {
      console.warn('To call getPageElement(...) pageNumber(greater than 0) is mandatory!');
      return;
    }
    var el = $('.page[page=' + pageNumber + ']').get(0);

    if (!el) {
      console.warn('Couldn\'t find the page!');
      return;
    }

    return el;
  });

  _.expose('highlightWord', function (el, start, end) {
    var txt = $(getTextNodesIn(el)).text(),
      startText = txt.substring(start),
      wordLength;

    if (!end || start >= end) {
      wordLength = new RegExp('[^' + wordDelimiters + ']*').exec(startText)[0].length;
      end = start + wordLength;
    }

    highlightRange(el, start, end);
  });

  _.expose('saveHighlight', function (indexId, params) {
    // if (indexId > -1) {
    //   var jsonStr = JSON.stringify(JSON.parse(_.highlighter.serializeHighlights()));

    //   localStorage.setItem('avagard_highlight_' + indexId, jsonStr);
    //   localStorage.setItem('avagard_highlight_params_' + indexId, JSON.stringify(params));
    // }
  });

  //54241 70

  _.removeHighlight = function (indexId) {
    if (indexId > -1) {
      localStorage.removeItem('avagard_highlight_' + indexId);
    }
  };

  _.expose('getHighlightInfo', function () {
    var selectedInfo = _.getSelectedRange();
    if (!selectedInfo.text.length) {
      _.alert('لطفا بخشی از متن را انتخاب نمایید!');
      return;
    }
    var
      startingBlock = $(selectedInfo.startNode),
      endingBlock = $(selectedInfo.endNode),
      startingPage = _.parseInt(startingBlock.closest('.page').attr('page')),
      endingPage = _.parseInt(endingBlock.closest('.page').attr('page')),
      startingIndex = 0,
      endingIndex = 0;

    startingIndex = $('#postswrapper div[page=' + startingPage + ']')
      .text()
      .indexOf($.trim(startingBlock.text()));

    endingIndex = $('#postswrapper div[page=' + endingPage + ']')
      .text()
      .indexOf($.trim(endingBlock.text()));

    return {
      bookId: _.currentBook,
      startingPage: startingPage,
      endingPage: endingPage,

      startingIndex: startingIndex,
      endingIndex: endingIndex
    };
  });

  // var pgText = $('#postswrapper .highlighted').closest('.page').text(),
  //   firstHighlightedText = $('#postswrapper .highlighted:last').closest('.page').attr('page'),
  //   firstHighlightedText = $('#postswrapper .highlighted:eq(0)').text(),
  //   highlightedText = $('#postswrapper .highlighted')
  //   .toArray()
  //   .reduce(function (val, current) {
  //     return (typeof val === 'string' ? val : $.trim($(val).text())) + $.trim($(current).text());
  //   });

}(this, jQuery));