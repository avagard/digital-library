(function ($) {
  'use strict';
  var arraySlice = Array.prototype.slice,
    cache = {},
    bookScroller;

  function getPage(url, callback) {
    if (!$.isFunction(callback)) {
      return;
    }

    if (typeof cache[url] === 'string') {
      callback(cache[url], 'success');
    } else {
      $.get(url, callback);
    }
  }

  function loadUrls(array, i, callback, dataCallback) {
    if (!$.isArray(callback.response)) {
      callback.response = [];
    }
    if (i < array.length) {
      getPage(array[i], function (data, status) {
        if (status !== 'success') {
          data = 'Not found!!';
        }

        if ($.isFunction(dataCallback)) {
          data = dataCallback(data);
        }

        callback.response.push(data);
        loadUrls(array, ++i, callback, dataCallback);
      });
    } else {
      return callback(callback.response);
    }
  }

  function loadPages(options, pageUrls, callback, dataCallback) {
    var pageContents = [];

    loadUrls(pageUrls, 0, function (data) {
      var k = 0,
        dataLen = data.length;

      for (; k < dataLen; k += 1) {
        pageContents.push('<div class="page" page="' + options.pages[k] + '">' + data[k] + '</div>');
      }

      if ($.isFunction(callback)) {
        callback(pageContents);
      }

    }, dataCallback);
  }

  function getUrl(options, pageNumber) {
    if (typeof options.urlMapper !== 'function') {
      if (typeof options.urlPattern === 'string') {
        options.urlMapper = (function (urlPattern) {
          return function (pageNumber) {
            return bookScroller.format(urlPattern, pageNumber);
          };
        }(options.urlPattern));
      } else {
        options.urlMapper = options.urlPattern;
      }
    }

    return options.urlMapper(pageNumber);
  }

  function getTopHeight(pageNumber) {
    var wrapper = $('.page:eq(0)').parent(),
      allPages = wrapper.find('>.page'),
      totalHeight = 0;

    _.some(allPages, function(el){
      var currentNumber = _.parseInt($(el).attr('page'));
      if(currentNumber < pageNumber){
        totalHeight += $(el).outerHeight();
      } else {
        return true;
      }
    });
    return totalHeight;
  }
  _.expose('getTopHeight', getTopHeight);

  function getCurrentPage(container) {
    var wrapper = $('.page:eq(0)').parent(),
      allPages = wrapper.find('>.page'),
      h = container.outerHeight(),
      cH = container.scrollTop() + h - parseInt(h / 2, 10),
      i = 0,
      ln = allPages.length,
      allH = 0,
      el;

    for (; i < ln; i += 1) {
      el = allPages.get(i);
      allH += $(el).outerHeight();
      if (allH >= cH) {
        break;
      }
    }

    return i;
  }

  $.getCurrentPage = getCurrentPage;

  function loadPage(options, pageNumber, before) {
    var scrollTop,
      loader = $(options.loader),
      wrapper = $(options.wrapper),
      container = $(options.container);

    loader.show();
    options.loading = true;
    getPage(getUrl(options, pageNumber), function (data, status) {
      if (status === 'success') {
        if ($.isFunction(options.dataCallback)) {
          data = options.dataCallback(data);
        }
        if (before) {
          options.pages = [pageNumber].concat(options.pages);
          scrollTop = container.scrollTop();
          $('<div class="page" page="' + pageNumber + '">' + data + '</div>').insertBefore(wrapper.find('.page:eq(0)'));
          container.scrollTop(scrollTop + options.pgOffset.top);
          options.initialPage = pageNumber;
        } else {
          options.pages.push(pageNumber);
          wrapper.append('<div class="page" page="' + pageNumber + '">' + data + '</div>');
        }
        loader.hide();
      } else {
        loader.html('<center>No more posts to show.</center>');
      }
      options.loading = false;
    });
  }

  function bindOnce(options, fn) {
    var container = $(options.container),
      data = $._data(container.get(0), 'events');

    if (!data || !data.scroll || !data.scroll.length) {
      container.scroll(fn);
    }
  }

  bookScroller = function bookScroller(options) {
    //page number is 1 indexed
    var pages = options.pages = [],
      urlMapper,
      loader = $(options.loader),
      wrapper = $(options.wrapper),
      container = $(options.container),
      next,
      isLast = false;

    window._options = options;

    if (typeof options.initialPage !== 'number' || options.initialPage < 1) {
      options.initialPage = 1;
    }

    if (options.initialPage === 1) {
      pages.push(1, 2);
    } else {

      pages.push(options.initialPage - 1, options.initialPage);
      next = options.initialPage + 1;
      if (next <= options.count) {
        pages.push(next);
      } else {
        isLast = true;
      }
    }

    if (typeof options.urlPattern === 'string') {
      urlMapper = (function (urlPattern) {
        return function (pageNumber) {
          return bookScroller.format(urlPattern, pageNumber);
        };
      }(options.urlPattern));
    } else {
      urlMapper = options.urlPattern;
    }

    options.loading = true;
    loadPages(options, pages.map(urlMapper), function (pageContents) {
      var pg0, pg1,
        ofst0, ofst1;

      wrapper.append(pageContents);

      if (pageContents.length > 2 || isLast) {
        container.animate({
            scrollTop: $('div.page:eq(1)').offset().top
          },
          'slow',
          function () {
            options.loading = false;
            if ($.isFunction(options.load)) {
              options.load();
              options.load = undefined;
            }
          });
      } else {
        options.loading = false;
      }

      pg0 = wrapper.find('.page:eq(0)');
      pg1 = wrapper.find('.page:eq(1)');

      ofst0 = pg0.offset();
      ofst1 = pg1.offset();

      options.gap = Math.abs(parseInt(ofst1.top - ofst0.top - pg0.outerHeight(), 10));
      options.pgOffset = pg1.offset();
    }, options.dataCallback);

    var isFirst = true;
    bindOnce(options, function (e) {

      if (options.loading) {
        e.preventDefault();
        return false;
      }

      var scrollTop = container.scrollTop(); //,
      //pgHeight = options.pgOffset.top,
      //pgScroll = scrollTop % pgHeight,
      //current = parseInt(scrollTop / pgHeight, 10),
      //pgH = pgHeight - options.gap - 40,
      var nextPage,
        previousPage,
        currentPageNumber;

      // if (pgScroll <= pgH) {
      //   options.currentPageIndex = current;
      // } else {
      //   options.currentPageIndex = current + 1;
      // }

      options.currentPageIndex = isFirst ? 0 : getCurrentPage(container);

      currentPageNumber = options.initialPage + options.currentPageIndex;
      if (currentPageNumber > options.count) {
        currentPageNumber = options.count;
      }
      _.updatePaging(currentPageNumber);

      if (scrollTop < 10) {

        previousPage = options.pages[options.currentPageIndex] - 1;
        if (previousPage > 0) {
          loadPage(options, previousPage, true);
        }

      } else if (scrollTop >= parseInt(wrapper.height() - container.height(), 10)) {

        nextPage = options.pages[options.currentPageIndex] + 1;
        if (nextPage <= options.count) {

          loadPage(options, nextPage);

        } else {
          loader.html('<center>پایان</center>');
          loader.show();
        }
      }
      isFirst = false;
    });

  };

  bookScroller.clear = function (options) {
    options.loading = false;
    options.pages = null;
  };

  bookScroller.format = function format(str, args) {
    if (arguments.length < 2) {
      return;
    }
    var len = arguments.length;
    if (len === 2 && $.isArray(args)) {
      return str.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] !== 'undefined' ? args[number] : match;
      });
    } else {
      return format(str, arraySlice.call(arguments, 1));
    }
  };

  $.bookScroller = bookScroller;

}(jQuery));