(function ($) {

  var BrowserDetect = {
    init: function () {
      this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
      this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "an unknown version";
      this.OS = this.searchString(this.dataOS) || "an unknown OS";
      this.highlightColor = this.getHighlightColor();
    },
    searchString: function (data) {
      for (var i = 0; i < data.length; i++) {
        var dataString = data[i].string;
        var dataProp = data[i].prop;
        this.versionSearchString = data[i].versionSearch || data[i].identity;
        if (dataString) {
          if (dataString.indexOf(data[i].subString) != -1)
            return data[i].identity;
        } else if (dataProp)
          return data[i].identity;
      }
    },
    searchVersion: function (dataString) {
      var index = dataString.indexOf(this.versionSearchString);
      if (index == -1) return;
      return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    },
    getHighlightColor: function () {
      /*
      Safari 6.0.3 Mac*: #B4D5FE
      Chrome 26.0.1410.65 Mac*: #ACCEF7
      Firefox 19.0 Mac*: #B4D5FF
      Chrome 26.0.1410.64 m Windows 8+: #3297FD
      Firefox 20.0.1 Windows 8+: #3399FF
      Safari 5.1.7 Windows 8+: #3298FD
      Internet Explorer 10.0.4 Windows 8+: #3399FF
      */
      var result;

      switch (this.browser) {

      case 'Safari':
        if (this.OS === 'Mac') {
          result = '#B4D5FE';
        } else {
          result = '#3298FD';
        }
        break;

      case 'Chrome':
        if (this.OS === 'Mac') {
          result = '#ACCEF7';
        } else {
          result = '#3297FD';
        }
        break;

      case 'Firefox':
        if (this.OS === 'Mac') {
          result = '#B4D5FF';
        } else {
          result = '#3399FF';
        }
        break;

      case 'MSIE':
      case 'Explorer':
        result = '#3399FF';
        break;

      default:
        result = '#ACCEF7';

      }

      return result;
    },
    dataBrowser: [{
      string: navigator.userAgent,
      subString: "Chrome",
      identity: "Chrome"
    }, {
      string: navigator.userAgent,
      subString: "OmniWeb",
      versionSearch: "OmniWeb/",
      identity: "OmniWeb"
    }, {
      string: navigator.vendor,
      subString: "Apple",
      identity: "Safari",
      versionSearch: "Version"
    }, {
      prop: window.opera,
      identity: "Opera"
    }, {
      string: navigator.vendor,
      subString: "iCab",
      identity: "iCab"
    }, {
      string: navigator.vendor,
      subString: "KDE",
      identity: "Konqueror"
    }, {
      string: navigator.userAgent,
      subString: "Firefox",
      identity: "Firefox"
    }, {
      string: navigator.vendor,
      subString: "Camino",
      identity: "Camino"
    }, { // for newer Netscapes (6+)
      string: navigator.userAgent,
      subString: "Netscape",
      identity: "Netscape"
    }, {
      string: navigator.userAgent,
      subString: "MSIE",
      identity: "Explorer",
      versionSearch: "MSIE"
    }, {
      string: navigator.userAgent,
      subString: "Gecko",
      identity: "Mozilla",
      versionSearch: "rv"
    }, { // for older Netscapes (4-)
      string: navigator.userAgent,
      subString: "Mozilla",
      identity: "Netscape",
      versionSearch: "Mozilla"
    }],
    dataOS: [{
      string: navigator.platform,
      subString: "Win",
      identity: "Windows"
    }, {
      string: navigator.platform,
      subString: "Mac",
      identity: "Mac"
    }, {
      string: navigator.userAgent,
      subString: "iPhone",
      identity: "iPhone/iPod"
    }, {
      string: navigator.platform,
      subString: "Linux",
      identity: "Linux"
    }]

  };

  BrowserDetect.init();

  $.client = {
    os: BrowserDetect.OS,
    browser: BrowserDetect.browser,
    highlightColor: BrowserDetect.highlightColor
  };

})(jQuery);
