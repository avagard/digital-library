(function (global, _, $) {

  var localStorage = global.localStorage;

  _.booksDocs = {};
  _.docsInfo = {};
  _.booksInfo = {};
  _.booksOptions = {};
  _.indexesInfo = {};
  _.pinpointsCache = {};

  _.getBookInfo = function getBookInfo(bookId, callback) {
    if (!Ext.isFunction(callback)) {
      console.warn('loadBookPages needs a callback to return the data');
      return;
    }

    var info = _.booksInfo[bookId],
      docs;

    if (typeof info === 'object') {
      docs = _.booksDocs[bookId];
      callback(info, docs.length);
      return;
    }

    _.ws.getPagesSorted(bookId, function (pages) {
      var i = 0,
        len = pages.length,
        bookInfo,
        docInfo;

      _.booksDocs[bookId] = pages;
      _.loadPaging();
      bookInfo = _.booksInfo[bookId] = {};

      for (; i < len; i += 1) {
        docInfo = {
          bookId: bookId,
          pageNumber: i + 1
        };
        bookInfo[i + 1] = bookId + '_' + (i + 1) + '_' + pages[i];
        _.docsInfo[pages[i]] = docInfo;
        localStorage.setItem('avagard_docInfo:' + pages[i], JSON.stringify(docInfo));
      }

      callback(bookInfo, len);
    });
  };

  _.loadPaging = function (pagingtoolbar) {
    if (!Ext.isNumber(_.currentBookId)) {
      return;
    }
    if (!pagingtoolbar) {
      pagingtoolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
    }
    var store = pagingtoolbar.getStore(),
      pages = _.booksDocs[_.currentBookId];
    if (store.totalCount !== pages.length) {
      store.totalCount = pages.length;
      store.pageSize = 1;
      store.loadData(pages);
      pagingtoolbar.onLoad();
      _.currentPage = store.currentPage;
      return store.currentPage;
    }
    return 0;
  };

  _.updatePaging = function (pageNumber) {
    if (_.currentPage !== pageNumber) {
      var pagingtoolbar = Ext.ComponentQuery.query('pagingtoolbar')[0],
        store = pagingtoolbar.getStore(),
        inputItem = pagingtoolbar.down('#inputItem');

      inputItem.setValue(pageNumber);
      store.currentPage = pageNumber;
      _.currentPage = store.currentPage;
    }
  };

  _.getDocumentId = function (bookId, pageNumber, callback) {
    var documentId = -1,
      docs = _.booksDocs[bookId];
    //_.booksDocs[52][67-1]
    if (docs) {
      documentId = docs[pageNumber - 1];

      if (Ext.isFunction(callback)) {
        setTimeout((function (callback, documentId) {
          return function () {
            callback(documentId);
          };
        }(callback, documentId)), 0);
      }

      return documentId;
    } else {
      _.getBookInfo(bookId, function () {

        _.getDocumentId(bookId, pageNumber, callback);

      });
    }
  };

  _.getDocumentInfo = function (documentId) {
    var info = _.docsInfo[documentId];
    if (info) {
      return _.docsInfo[documentId];
    } else if ((info = localStorage.getItem('avagard_docInfo:' + documentId))) {
      info = JSON.parse(info);
      _.docsInfo[documentId] = info;
      return info;
    }
    return null;
  };

  _.getBookPageUrl = function getBookPageUrl(bookId, pageNumber) {
    var bookInfo = _.booksInfo[bookId],
      bookPageId;

    if (typeof bookInfo !== 'object') {
      console.warn('getBookPageUrl needs to wait for bookpage info');
      return;
    }

    bookPageId = bookInfo[pageNumber];

    if (typeof bookPageId !== 'string') {
      console.warn('there is no such a page in the book');
      return;
    }

    return '/RTFHandler.ashx?userID=' + _.uid() + '&arg=' + bookPageId;
  };

  _.loadBook = function loadBook(bookId, pageNumber, extraInfo) {
    var indexId = extraInfo && extraInfo.indexId;

    if (indexId === undefined) {
      indexId = -1;
    }

    if (!Ext.isNumber(bookId) && Ext.isNumeric(bookId)) {
      bookId = parseInt(bookId, 10);
    }
    _.currentBookId = bookId;
    _.getBookInfo(bookId, function (info, count) {
      var options = _.booksOptions[bookId],
        urlPattern;

      if (!options) {
        urlPattern = (function (bookId) {
          return function (pageNumber) {
            return _.getBookPageUrl(bookId, pageNumber);
          };
        }(bookId));

        options = {
          initialPage: 1,
          container: '.container',
          wrapper: '#postswrapper',
          loader: 'div#loadmoreajaxloader',
          count: count,
          urlPattern: urlPattern,
          dataCallback: function (data) {
            return data.replace('text-align:Left;', '');
          }
        };

        if (extraInfo) {
          options.extraInfo = extraInfo;
        }

        _.booksOptions[bookId] = options;
      }
      _.loadBookMedia(bookId, pageNumber);
      _.loadBookFrame(bookId, pageNumber, indexId);
    });
  };

  _.loadBookColorized = function loadBook(bookId, pageNumber, colorizationInfo) {

    console.log('colorizationInfo:' + colorizationInfo);
    var info = {
      /*
  colorizationInfo.ID: 24,
  colorizationInfo.BookTile: "صحيفه امام - جلد ششم",
  colorizationInfo.DocumentTitle: "0051",
  colorizationInfo.Title: null,
  colorizationInfo.Type: 2
    */
    };

    _.loadBook(bookId, pageNumber, info);
  };

  _.loadBookFrame = function (bookId, pageNumber, indexId) {
    //var data = result.responseText;
    var panel = Ext.ComponentQuery.query('#centerTabpanel')[0];
    panel.setActiveTab(0);
    var container = document.querySelector('#bookDetailPanel-innerCt'),
      frame;
    //_.rtfdata = data;
    if (!pageNumber) {
      pageNumber = 1;
    }
    if (!(frame = container.querySelector('iframe'))) {
      frame = document.createElement('iframe');
      frame.style.border = 'solid 0px transparent';
      frame.style.width = '100%';
      frame.style.height = '100%';
      container.style.overflow = 'hidden';
      container.appendChild(frame);
    }

    frame.src = '/book-scroller/?' + (new Date().valueOf()) + '#' + bookId + '_' + pageNumber + '_' + indexId;
  };

  _.loadBookMedia = function (bookId, pageNumber) {
    return _.ws.getBookPageMedia(bookId, pageNumber)
      .then(function (data) {
        var i = 0,
          len = data.length;

        for (; i < len; i += 1) {
          data[i].bookId = bookId;
          data[i].pageNumber = pageNumber;
        }

        Ext.getStore('DocumentMediaViewStore').loadData(data, false);

        return data[0];
      });
  };

  _.getIndexParams = function (indexId) {
    if (indexId > -1) {
      var jsonStr = localStorage.getItem('avagard_highlight_params_' + indexId),
        params = JSON.parse(jsonStr);
      return params;
    }
    return null;
  };

  _.loadIndex = function (indexId) {
    _.ws.getIndexById(indexId)
      .done(function (data) {
        _.getBookInfo(data.BookID, function (bookInfo, count) {
          var startDocInfo = _.getDocumentInfo(data.StartDocumentID),
            endDocInfo = _.getDocumentInfo(data.EndDocumentID),
            info = {
              bookId: startDocInfo.bookId,
              startingPage: startDocInfo.pageNumber,
              startPosition: data.StartPosition,
              endingPage: endDocInfo.pageNumber,
              endPosition: data.EndPosition
            };
          _.indexesInfo[indexId] = info;
          _.loadBook(info.bookId, info.startingPage, {
            indexId: indexId
          });
        });
      })
      .fail(function (xhr, status, message) {
        _.alert(message);
      });
  };

  _.highlightSavedIndex = function () {
    var
      tabPanel = Ext.ComponentQuery.query('#indexTabPanel')[0],
      bookTree = tabPanel.getActiveTab().down('indexBookTree'),
      selection = bookTree.getSelectionModel().getSelection(),
      node,
      indexId;

    if (selection.length) {
      node = selection[0];
      indexId = node.get('id');

      indexId = Number.parseInt(indexId, 10);

      if (Number.isNaN(indexId)) {
        _.alert('آیتم مورد نظر در درخت نمایه معتبر نمی باشد!');
        return false;
      }

      //FIXME: Mehran: look for the old _.getIndexParams(indexId) function and clean up the code
      _.loadIndex(indexId);
    } else {
      _.alert('لطفا یکی از آیتم درخت نمایه را انتخاب نمایید!');
      return false;
    }

  };

  _.addIndex = function (data) {
    var node = data,
      nodeId,
      params;

    if (data) {
      nodeId = _.parseInt(node.id);

      if (_.isNaN(nodeId)) {
        _.alert('آیتم مورد نظر در درخت نمایه برای افزودن نمایه معتبر نمی باشد!');
        return false;
      }

      params = _.getHighlightInfo();
      params.nodeId = nodeId;

      _.getInput(function (title) {
        params.title = title;
        _.ws.addNewIndex(params, function (response) {

          var indexId = Number.parseInt(response);
          if (Number.isNaN(indexId)) {
            console.warn('invalid indexId returned from webservice');
            return;
          }
          _.saveHighlight(indexId, params);

        });
      });
    } else {
      _.alert('لطفا یکی از آیتم درخت نمایه را انتخاب نمایید!');
      return false;
    }
  };

  _.isReady = function (id) {
    // if (id === 'bookDetailPanel') {
    // }
  };

  _.getRawItem = function (item) {
    return item.raw;
  };

  _.getRawList = function (list) {
    return _.map(list.items, _.getRawItem);
  };

  _.getRawPinpoint = function (item) {
    return {
      pageNumber: _.parseInt(item.get('ID')),
      position: _.parseInt(item.get('Position') || 0)
    };
  };

  _.getRawPinpoints = function (data) {
    return _.map(data.items, _.getRawPinpoint);
  };

  _.cacheAllPinpoints = function (store) {
    var data = store.data,
      items = _.isObject(data) && data.items;
    _.each(items, function(item){
      var pageNumber = item.get('ID'),
        bookId = item.get('BookID'),
        cache = _.pinpointsCache[bookId],
        position = _.parseInt(item.get('Position') || 0);

      if(!cache){
        cache = _.pinpointsCache[bookId] = {};
      }

      if(!cache[pageNumber]){
        cache[pageNumber] = [];
      }

      cache[pageNumber].push(position);
    });
  };

  _.loadBookmarkedBook = function loadBookmarkedBook(bookId, pageNumber, item, data) {
    // var pinpoints = _.pinpointsCache[bookId];
    // if (!pinpoints) {
    //   pinpoints = _.getRawPinpoints(data);
    //   _.pinpointsCache[bookId] = pinpoints;
    // }
    var info = {
      /*
      bookMarkInfo.BookTile,
      bookMarkInfo.DocumentTitle,
      bookMarkInfo.ID
,     bookMarkInfo.BookID,
      bookMarkInfo.Title,
      bookMarkInfo.Type
     */
      item: _.getRawPinpoint(item)
      //pinpoints: pinpoints
    };
    _.loadBook(bookId, pageNumber, info);

  };
  _.loadCommentedBook = function loadCommentedBook(bookId, pageNumber, commentedInfo) {
    var info = {
      /*
      commentedInfo.ID: 24,
      commentedInfo.BookTile: "صحيفه امام - جلد ششم",
      commentedInfo.DocumentTitle: "0051",
      commentedInfo.Title: null,
      commentedInfo.Type: 2
    */
    };
    _.loadBook(bookId, pageNumber, info);

  };
  _.loadBookColorized = function loadBookColorized(bookId, pageNumber, colorizationInfo) {
    var info = {
      /*
    colorizationInfo.ID: 24,
    colorizationInfo.BookTile: "صحيفه امام - جلد ششم",
    colorizationInfo.DocumentTitle: "0051",
    colorizationInfo.Title: null,
    colorizationInfo.Type: 2
    */
    };

    _.loadBook(bookId, pageNumber, info);
  };
}(this, _, jQuery));