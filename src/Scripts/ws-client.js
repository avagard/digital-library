(function (global, $) {

  var _ = global._;
  if (_.ws === undefined) {
    _.ws = {
      urls: {}
    };
  }

  var urls = {
    getIndexTabs: '/WebServices/DigitalLibraryRESTService.svc/GetIndexTabs/' + _.uid(),
    getDocumentAsText: '/WebServices/DigitalLibraryRESTService.svc/GetDocumentAsText/' + _.uid() + '/',
    addNewIndex: '/WebServices/IndexService.svc/AddNewIndex/' + _.uid() + '/',
    getIndexById: '/WebServices/IndexService.svc/GetSimpleIndexByID/' + _.uid() + '/',
    getPagesSorted: '/WebServices/BookService.svc/GetPagesSorted/' + _.uid() + '/',
    getMediaByDocument: '/WebServices/DigitalLibraryRESTService.svc/GetMediaByDocument/' + _.uid() + '/',
    addToUserLibrary: '/WebServices/BookService.svc/',
    getBookProperties: '/WebServices/BookService.svc/GetBookProperties/' + _.uid() + '/',
    getPDFUrl: '/WebServices/BookService.svc/GetPDFURL/' + _.uid() + '/',
    addCategory: '/WebServices/BookService.svc/AddCategory/' + _.uid() + '/',
    deleteCategory: '/WebServices/BookService.svc/',
    renameCategory: '/WebServices/BookService.svc/RenameCategory/' + _.uid() + '/',
    addNewNode: '/WebServices/BookService.svc/AddNewNode/' + _.uid() + '/گره جدید',

    //FIXME: amin: why we have two addNewIndex here
    //addNewIndex: '/WebServices/IndexService.svc/AddNewNode/' + _.uid() + '/',

    deleteEmphasize: '/WebServices/DocumentService.svc/DeleteEmphasize/' + _.uid() + '/',
    deleteBookmarkedNode: '/WebServices/DocumentService.svc/DeleteEmphasize/' + _.uid() + '/',
    filterIndexPathInTree: '/WebServices/IndexService.svc/FilterIndexPathInTree/' + _.uid() + '/',
    stampBook: '/WebServices/BookService.svc/StampBook/' + _.uid() + '/',
    hasPDF: '/WebServices/BookService.svc/HasPDF/' + _.uid() + '/',
    getBookStamp: '/WebServices/BookService.svc/GetBookStamp/' + _.uid() + '/',
    getTags: '/WebServices/DocumentService.svc/GetTagsByDocumentAndMedia/' + _.uid() + '/',
    GetTagsByMedia: '/WebServices/MediaService.svc/GetTagsByMedia/' + _.uid() + '/',
    addHighLight: '/WebServices/DocumentService.svc/AddHighLight/' + _.uid() + '/',
    addBookMark: '/WebServices/DocumentService.svc/AddBookmark/' + _.uid() + '/',
    addComment: '/WebServices/DocumentService.svc/AddComment/' + _.uid() + '/',
    addUnderline: '/WebServices/DocumentService.svc/AddUnderline/' + _.uid() + '/',
    addStrikethrough: '/WebServices/DocumentService.svc/AddStrikethrough/' + _.uid() + '/'
  };

  _.ws.urls = urls;
  _.ws.cache = {};

  _.ws.getIndexTabs = function (callback) {
    var url = urls.getIndexTabs,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[getIndexTabs]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[getIndexTabs][' + message + ']');
    });

    return promise;
  };

  /*
  1 - /DigitalLibraryRESTService.svc/GetDocumentAsText/1/4050
2 - /IndexService.svc/AddNewIndex/1/2627/TitleOfIndex/4050/20/4052/30
3 - /IndexService.svc/GetSimpleIndexByID/1/35046
  */

  _.ws.getDocumentAsText = function (documentId, callback) {
    var url = urls.getDocumentAsText + documentId;

    return $.get(url)
      .done(function (data, status) {
        if (status === 'success') {
          if ($.isFunction(callback)) {
            callback(data);
          }
        } else {
          console.error('Unsuccessful webservice call:[getDocumentAsText]');
        }
      })
      .fail(function (xhr, status, message) {
        console.error('Error on webservice call:[getDocumentAsText][' + message + ']');
      });

  };

  /*
  _.ws.addNewIndex({
      bookId: 52,
      nodeId: 2627,
      title: 'TitleOfIndex',
      startingPage: 4050,
      startingIndex: 20,
      endingPage: 4052,
      endingIndex: 30
    }, function(response){
      //manipulate
    });
  */
  _.ws.addNewIndex = function (params, callback) {

    var startingPageId = _.getDocumentId(params.bookId, params.startingPage),
      endingPageId = (params.startingPage === params.endingPage) ?
      startingPageId :
      _.getDocumentId(params.bookId, params.endingPage);

    if (startingPageId === -1 || endingPageId === -1) {
      _.alert('Unable to find the page document');
    }

    var url = urls.addNewIndex +
      params.nodeId + '/' +
      params.title + '/' +
      startingPageId + '/' +
      params.startingIndex + '/' +
      endingPageId + '/' +
      params.endingIndex;

    return $.get(url)
      .done(function (data, status) {
        if (status === 'success') {
          if ($.isFunction(callback)) {
            callback(data);
          }
        } else {
          console.error('Unsuccessful webservice call:[addNewIndex]');
        }
      })
      .fail(function (xhr, status, message) {
        console.error('Error on webservice call:[addNewIndex][' + message + ']');
      });
  };

  /*
    _.ws.getIndexById(35046, function(response){
      //manipulate
    });

    response:
    {
      'EndDocumentID':4089,
      'EndPosition':231,
      'StartDocumentID':4088,
      'StartPosition':1330
    }
  */
  _.ws.getIndexById = function (indexId, callback) {
    var url = urls.getIndexById + indexId;

    return $.get(url)
      .done(function (data, status) {
        if (status === 'success') {
          if ($.isFunction(callback)) {
            callback(data);
          }
        } else {
          console.error('Unsuccessful webservice call:[getIndexById]');
        }
      })
      .fail(function (xhr, status, message) {
        console.error('Error on webservice call:[getIndexById][' + message + ']');
      });
  };

  _.ws.getPagesSorted = function (bookId, callback) {
    var url = urls.getPagesSorted + bookId,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[getPagesSorted]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[getPagesSorted][' + message + ']');
    });

    return promise;
  };

  _.ws.getMediaByDocument = function (documentId, callback) {
    var url = urls.getMediaByDocument + documentId,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[getMediaByDocument]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[getMediaByDocument][' + message + ']');
    });

    return promise;
  };
  _.ws.getBookStamp = function (nodeId, callback) {

    var url = urls.getBookStamp + nodeId,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[getBookStamp]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[getBookStamp][' + message + ']');
    });

    return promise;

  };
  _.ws.getBookProperties = function (objectId, callback) {
    var url = urls.getBookProperties + objectId,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[getBookProperties]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[getBookProperties][' + message + ']');
    });

    return promise;
  };

  _.ws.addToUserLibrary = function (nodeType, id, text, callback) {

    var type;
    if (nodeType === 0) {
      type = 'AddCategoryToUserLibrary/';
      //     secondParams=;
    } else if (nodeType === 1) {
      type = 'AddBookToUserLibrary/';
      //       secondParams=;
    }
    var url = urls.addToUserLibrary + type +
      _.uid() + '/' + id + '/' + text,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[addToUserLibrary]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[addToUserLibrary][' + message + ']');
    });

    return promise;
  };

  _.ws.getPDFUrl = function (objectId, callback) {
    var url = urls.getPDFUrl + objectId,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[getPDFUrl]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[getPDFUrl][' + message + ']');
    });

    return promise;
  };
  _.ws.addCategory = function (scope, catTitle, callback) {
    var url = urls.addCategory + catTitle,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(scope, data);
        } else {
          console.error('Unsuccessful webservice call:[addCategory]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[addCategory][' + message + ']');
    });

    return promise;
  };
  _.ws.renameCategory = function (tree, item, oldName, id, name, callback, errorCallback) {

    var url = urls.renameCategory + id + '/' + name,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          errorCallback(tree, item, oldName);
          console.error('Unsuccessful webservice call:[renameCategory]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[renameCategory][' + message + ']');
    });

    return promise;


  };
  _.ws.deleteCategory = function (tree, item, cls, nodeId, parentId, callback, errorCallback) {
    var methodName;
    if (cls === 'subject-node') {

      methodName = 'DeleteCategory/' + _.uid() + '/' + nodeId.slice(4);
    } else if (cls === 'book-node') {

      methodName = 'DeleteBookInCategory/' + _.uid() + '/' + nodeId + '/' + parentId;
    }
    var url = urls.deleteCategory + methodName,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(item, data);
        } else {
          errorCallback(tree, item);
          console.error('Unsuccessful webservice call:[deleteCategory]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[deleteCategory][' + message + ']');
    });

    return promise;
  };
  _.ws.addNewNode = function (callback) {

    var url = urls.addNewNode,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[addNewNode]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[addNewNode][' + message + ']');
    });

    return promise;
  };

  _.ws.addnewIndexNode = function (text, parentId, callback) {
    var url = urls.addNewIndex + text + '/' + parentId + '/0',
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[addnewIndexNode]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[addnewIndexNode][' + message + ']');
    });

    return promise;
  };
  _.ws.deleteBookmarkedNode = function (id, callback) {

    var url = urls.deleteBookmarkedNode + id,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[deleteBookmarkedNode]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[deleteBookmarkedNode][' + message + ']');
    });

    return promise;
  };

  _.ws.deleteEmphasize = function (id, callback) {
    var url = urls.deleteEmphasize + id,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[deleteEmphasize]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[deleteEmphasize][' + message + ']');
    });

    return promise;
  };

  _.ws.filterIndexPathInTree = function (value, tabId, callback) {

    var url = urls.filterIndexPathInTree + value + '/' + tabId,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(tabId, data);
        } else {
          console.error('Unsuccessful webservice call:[filterIndexPathInTree]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[filterIndexPathInTree][' + message + ']');
    });

    return promise;


  };
  _.ws.stampBook = function (id, recordData, callback) {

    var url = urls.stampBook + id + '/' + recordData,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          console.error('successful webservice call:[stampBook]:' + data);
        } else {
          console.error('Unsuccessful webservice call:[stampBook]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[stampBook][' + message + ']');
    });

    return promise;


  };
  _.ws.hasPDF = function (nodeId, callback) {

    var url = urls.hasPDF + nodeId,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
          console.error('successful webservice call:[hasPDF]:' + data);
        } else {
          console.error('Unsuccessful webservice call:[hasPDF]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[hasPDF][' + message + ']');
    });

    return promise;

  };


  _.ws.getBookPageMedia = function (bookId, pageNumber, callback) {
    var documentId = _.getDocumentId(bookId, pageNumber);

    return _.ws.getMediaByDocument(documentId, callback);
  };

  _.ws.callExport = function (type, selected) {

    var exportUrl = '/WebServices/DigitalLibraryExportedFileHandler.ashx?UserID=' + _.uid() + '&ExportType=' +
      type.type;
    if (selected !== undefined) {
      var nodes = '';
      for (var i = selected.length - 1; i >= 0; i--) {
        nodes += selected[i].data.id + ',';
        nodes += selected[i].isExpanded() + ',false';

      }

      exportUrl += '&SelectedNodes=' + nodes;

    }
    var hiddenForm = Ext.create('Ext.form.Panel', {
      title: 'hiddenForm',
      standardSubmit: true,
      url: exportUrl,
      timeout: 120000,
      height: 0,
      width: 0,
      hidden: true,
      items: [{
          xtype: 'hiddenField',
          name: 'field1',
          value: 'field1Value'
        },
        // additional fields
      ]
    });
    hiddenForm.getForm().submit();
  };

  //_.ws.getTags(41562, 43)
  _.ws.getTags = function (documentId, mediaId, callback) {
    //41562/43
    var url = urls.getTags + documentId + '/' + mediaId,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
          console.error('successful webservice call:[getTags]:' + data);
        } else {
          console.error('Unsuccessful webservice call:[getTags]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[getTags][' + message + ']');
    });

    return promise;
  };

  _.ws.getPageTags = function (bookId, pageNumber, mediaId, callback) {
    var documentId = _.getDocumentId(bookId, pageNumber);
    return _.ws.getTags(documentId, mediaId, callback);
  };

  /*

_.ws.addHighLight(41562,20,10,function(arg){debugger;})


*/
  _.ws.addHighLight = function (id, startPosition, length, callback) {
    var url = urls.addHighLight + id + '/' + startPosition + '/' + length,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[addHighLight]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[addHighLight][' + message + ']');
    });

    return promise;
  };
  /*

_.ws.addUnderline(41562,20,10,function(arg){debugger;})


*/
  _.ws.addUnderline = function (id, startPosition, length, callback) {
    var url = urls.addUnderline + id + '/' + startPosition + '/' + length,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[addUnderline]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[addUnderline][' + message + ']');
    });

    return promise;
  };
  /*

  _.ws.addStrikethrough(41562,20,10,function(arg){debugger;})


*/

  _.ws.addStrikethrough = function (id, startPosition, length, callback) {
    var url = urls.addStrikethrough + id + '/' + startPosition + '/' + length,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[addStrikethrough]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[addStrikethrough][' + message + ']');
    });

    return promise;
  };

  /*
_.ws.addBookMark(41562,55,function(arg){debugger;})
*/
  _.ws.addBookMark = function (id, position, callback) {
    var url = urls.addBookMark + id + '/' + position,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[addBookMark]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[addBookMark][' + message + ']');
    });

    return promise;
  };

  /*
_.ws.addComment(41562,65,'hello',function(arg){debugger;})
*/
  _.ws.addComment = function (id, position, title, callback) {
    var url = urls.addComment + id + '/' + position + '/' + btoa(encodeURIComponent('title')),
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
        } else {
          console.error('Unsuccessful webservice call:[addComment]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[addComment][' + message + ']');
    });

    return promise;
  };
  _.ws.getTagsByMedia = function (mediaId, bookId, callback) {
    var url = urls.GetTagsByMedia + mediaId + '/' + bookId,
      promise = $.get(url);

    if ($.isFunction(callback)) {
      promise.done(function (data, status) {
        if (status === 'success') {
          callback(data);
          console.error('successful webservice call:[GetTagsByMedia]:' + data);
        } else {
          console.error('Unsuccessful webservice call:[GetTagsByMedia]');
        }
      });
    }

    promise.fail(function (xhr, status, message) {
      console.error('Error on webservice call:[GetTagsByMedia][' + message + ']');
    });

    return promise;
  };

}(this, jQuery));