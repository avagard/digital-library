var bookSearchDataView = [
  ['1', 'صحیفه سجادیه جلد اول -38', 'صحیفه امام-فارسی مجله',
    '؛ اكثر مسلمانان در عرصه‏هاى مختلف حيات اجتماعى از فرهنگ و سياست و نظام حقوقى گرفته تا آداب و روابط اجتماعى و نظامهاى آموزشى خويش آنچنان مجذوب و مرعوب فرهنگهاى غربى و شرقى و وارداتى شده بودند كه احساس هويت مستقل و مشترك و اسلامى به شدت در مقابل نفوذ ايسمها و گرايشهاى سياسى غير دينى رنگ باخته بی هویتی فره',
    '1'
  ],
  ['2', 'صحیفه سجادیه جلد اول -38', 'صحیفه امام-فارسی مجله',
    '؛ اكثر مسلمانان در عرصه‏هاى مختلف حيات اجتماعى از فرهنگ و سياست و نظام حقوقى گرفته تا آداب و روابط اجتماعى و نظامهاى آموزشى خويش آنچنان مجذوب و مرعوب فرهنگهاى غربى و شرقى و وارداتى شده بودند كه احساس هويت مستقل و مشترك و اسلامى به شدت در مقابل نفوذ ايسمها و گرايشهاى سياسى غير دينى رنگ باخته بی هویتی فره',
    '2'
  ],
  ['3', 'صحیفه سجادیه جلد اول -38', 'صحیفه امام-فارسی مجله',
    '؛ اكثر مسلمانان در عرصه‏هاى مختلف حيات اجتماعى از فرهنگ و سياست و نظام حقوقى گرفته تا آداب و روابط اجتماعى و نظامهاى آموزشى خويش آنچنان مجذوب و مرعوب فرهنگهاى غربى و شرقى و وارداتى شده بودند كه احساس هويت مستقل و مشترك و اسلامى به شدت در مقابل نفوذ ايسمها و گرايشهاى سياسى غير دينى رنگ باخته بی هویتی فره',
    '3'
  ],
  ['4', 'صحیفه سجادیه جلد اول -38', 'صحیفه امام-فارسی مجله',
    '؛ اكثر مسلمانان در عرصه‏هاى مختلف حيات اجتماعى از فرهنگ و سياست و نظام حقوقى گرفته تا آداب و روابط اجتماعى و نظامهاى آموزشى خويش آنچنان مجذوب و مرعوب فرهنگهاى غربى و شرقى و وارداتى شده بودند كه احساس هويت مستقل و مشترك و اسلامى به شدت در مقابل نفوذ ايسمها و گرايشهاى سياسى غير دينى رنگ باخته بی هویتی فره',
    '4'
  ],
  ['5', 'صحیفه سجادیه جلد اول -38', 'صحیفه امام-فارسی مجله',
    '؛ اكثر مسلمانان در عرصه‏هاى مختلف حيات اجتماعى از فرهنگ و سياست و نظام حقوقى گرفته تا آداب و روابط اجتماعى و نظامهاى آموزشى خويش آنچنان مجذوب و مرعوب فرهنگهاى غربى و شرقى و وارداتى شده بودند كه احساس هويت مستقل و مشترك و اسلامى به شدت در مقابل نفوذ ايسمها و گرايشهاى سياسى غير دينى رنگ باخته بی هویتی فره',
    '5'
  ]
];