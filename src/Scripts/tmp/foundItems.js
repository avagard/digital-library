var x = {
  "FoundItems": [{
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "والشيعة \/ موقع الاقليات الدينية \/ دعم الجيش الحاضرون: اساتذة وطلاب الحوزة العلمية في قم ومنتسبو شركة الخطوط الجوية الوطنية وشركة النفط أعوذ بالله من الشيطان الرجيم بسم الله الرحمن الرحيم دولة <b>امام<\/b> العصر والزمان أعرب عن شكري لجميع فئات الشعب، اشكركم على هذا التضامن والمساعي الموحدة التي تمت  بين حكومتنا وبين شعبنا. ان نجاحكم مرهون بتضامنكم. انني اعرب عن شكري لما اعلنه علماء  الدين من التضامن مع",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "70"
  }, {
    "BookAuthor":           "",
    "BookCoverImagePath":   "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF":           false,
    "BookID":               138,
    "BookPreviewText":        "الحاضرون: جمع من منتسبي القوتين الجوية والبرية. اعوذ بالله من الشيطان الرجيم بسم الله الرحمن الرحيم جنود <b>امام<\/b> العصر \"عج\" في خدمت القرآن تحية لكم ياجند <b>امام<\/b> العصر - سلام الله عليه -. وكما قلتم في شعاراتكم فقد كنتم حتى  وقت قريب في خدمة الطاغوت() الذي قضى على كل وجودنا وافرغ كل خزائننا وجعلنا عبيدا  للاجانب. ومن اليوم اصبحتم في خدمة <b>امام<\/b> العصر - سلام الله عليه - وفي خدمة القرآن الكريم  الذي ضمن",
    "BookPublishYear":      "",
    "BookPublisher":        null,
    "BookSubject":          "صحیفه امام - عربی",
    "BookTitle":            "صحیفه عربی- جلد ششم",
    "PageNumberInBook":       "86"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=52&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 52,
    "BookPreviewText": "سخنرانى زمان: 12 بهمن 1357 \/ 3 ربيع‏الاول 1399 مکان: تهران، فرودگاه مهرآباد موضوع: سخنرانى تاريخى <b>امام<\/b> خمينى در بيان چگونگى جريان انقلاب مناسبت: بازگشت <b>امام<\/b> خمينى به ايران پس از پانزده سال تبعيد حضار: ميليونها تن از مردم  بسم‏اللّه‏ الرحمن الرحيم تشكر از همۀ طبقات      من از عواطف طبقات مختلف ملت تشكر مى‏كنم. عواطف ملت ايران به دوش من بار گرانى است كه نمى‏توانم جبران كنم. من از طبقۀ روحانيون كه در",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - فارسی",
    "BookTitle": "صحيفه امام - جلد ششم",
    "PageNumberInBook": "36"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=52&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 52,
    "BookPreviewText": "بكنيم. و از خداى تبارك و تعالى مى‏خواهم كه توفيق بدهد به آقاى مهندس بازرگان كه اين مأموريت را به وجه احسن انجام بدهد.  پاسخ به پرسشهاى خبرنگاران  [پس از سخنان <b>امام<\/b> خمينى و معرفى نخست‏وزير دولت موقت، خبرنگاران پرسشهاى خود را مطرح كردند. <b>امام<\/b> به اين پرسشها با اختصار پاسخ دادند. متن سؤال و جوابها به شرح زير است:] سؤال: [حضرت آيت‏اللّه‏ فرموديد كسانى كه اين دولت را نپذيرند مجازات خواهند شد؛ اگر ارتش",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - فارسی",
    "BookTitle": "صحيفه امام - جلد ششم",
    "PageNumberInBook": "87"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "الشاه) مخلوقا لو انه امهل اكثر من ذلك لما تورع عن ادعاء الفرعونية والإلهية، إلا انه تصاغر  <b>امام<\/b> الشعب ووقف بمنتهى الذلة يستغفر ويطلب العفو ولم يقبل الشعب منه ذلك، قائلاً: كلا،  عليك ان ترحل!. والآن وحينما رحل فان الامريكان بدورهم لم يقبلوه وهو موجود حاليا في  الرباط يعيش هناك متجولا وبحالة من التوتر والانهيار، اي انكم اصبتموه بالتوتر والانهيار،  صفعتموه فاصيب بالهستيريا. والآن ايضا انتم مطالبون",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "74"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=52&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 52,
    "BookPreviewText": "سخنرانى زمان: 12 بهمن 1357 \/ 3 ربيع‏الاول 1399 مکان: تهران، بهشت زهرا موضوع: غيرقانونى بودن مجلس و دولت منصوب شاه و مفاسد رژيم مناسبت: بازگشت <b>امام<\/b> خمينى به ايران پس از پانزده سال تبعيد حضار: ميليونها نفراز مردم تهران و شهرهاى مختلف ايران اعوذ باللّه‏ من الشيطان الرجيم بسم اللّه‏ الرحمن الرحيم تشكر و تسليت       ما در اين مدت مصيبتها ديديم؛ مصيبتهاى بسيار بزرگ. و بعضِ پيروزيها حاصل شد كه البته آن هم",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - فارسی",
    "BookTitle": "صحيفه امام - جلد ششم",
    "PageNumberInBook": "38"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=52&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 52,
    "BookPreviewText": "آزادى مى‏خواهيم ما. ما پنجاه سال است كه در اختناق به سر برديم. نه مطبوعات داشتيم، نه راديو صحيح داشتيم، نه تلويزيون صحيح داشتيم؛ نه خطيب مى‏توانست حرف بزند، نه اهل منبر مى‏توانستند حرف بزنند، نه <b>امام<\/b> جماعت مى‏توانست آزاد كار خودش را ادامه بدهد؛ نه هيچ يك از اقشار ملت كارشان را مى‏توانستند ادامه بدهند. و در زمان ايشان هم همين اختناق به طريق بالاتر باقى است و باقى بود. و الآن هم باز نيمۀ حشاشۀ  او كه",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - فارسی",
    "BookTitle": "صحيفه امام - جلد ششم",
    "PageNumberInBook": "44"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=52&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 52,
    "BookPreviewText": "اللّه‏ الرحمن الرحيم تحريف اسلام      من از آقايان علما و افاضل بايد تشكر كنم. پيروزى ملت مرهون اقدامات علما ـ اولاً ـ و ساير طبقات ـ ثانياً ـ بوده است. شما علما همان طورى كه وظيفۀ شرعى‏تان هست كه <b>امام<\/b> امت باشيد، پيشقدم باشيد در مسائل امت، دفع كنيد مفاسد را از ملتها، بحمداللّه‏ قيام به امر فرموديد. و من از قِبَل ملت شريف ايران از شما تشكر مى‏كنم. خداوند ان‏شاءاللّه‏ روحانيت را كه ذخيرۀ ملت است،",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - فارسی",
    "BookTitle": "صحيفه امام - جلد ششم",
    "PageNumberInBook": "68"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=52&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 52,
    "BookPreviewText": "قيصر و محراب مال آخوند! ـ محراب هم نمى‏گذارند براى ما باشد ـ اسلام دين سياست است؛ حكومت دارد. شما بخشنامۀ حضرت امير و كتاب حضرت امير به مالك اشتر را بخوانيد ببينيد چيست. دستورهاى پيغمبر و دستورهاى <b>امام<\/b> ـ عليه السلام ـ در جنگها و در سياسات ببينيد چه دارد. اين ذخاير را ما داريم، عُرضۀ استفاده‏اش را نداريم. ذخيره موجود است، همه چيز داريم، عُرضۀ استفاده نداريم. مثل اينكه ايران همه چيز دارد اما مى‏دهند",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - فارسی",
    "BookTitle": "صحيفه امام - جلد ششم",
    "PageNumberInBook": "71"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "سياسيا ولا طالبا جامعيا ولن تبقى لنا  مدرسة فيضية ولا غيرها. هذه مؤامرة ونحن الآن مبتلون بمؤامرة أخرى. مؤامرات متنوعة لقد رأيتم ومنذ البداية انهم يحاولون خداعنا بمختلف الاشكال. يأتي أحدهم ليقف <b>امام<\/b>  الناس ويقول “أيها العلماء الأعلام والمراجع العظام، أيها الكذا والكذا لقد ارتكبنا بعض الأخطاء ثم  عدنا عنها الآن”! انه يخاطب ذات العلماء الأعلام والمراجع العظام الذين وصفهم في مناسبة أخرى  بانهم ",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "31"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "ما شاء الله، من أجل ماذا؟ من أجل اقامة الحق والعدل. علينا نحن ايضاً ان نقيم العدل وليس عذرا ان نقول باننا لا نمتلك القوة لذلك، فهذا الشعب  هو قوتنا، وابناء هذا الشعب قد وقفوا بقبضاتهم الخالية <b>امام<\/b> المدافع والدبابات وقدموا القتلى  ايضا، ونحن طبعا نفتخر بهم ونترحم عليهم وسنقتل نحن ايضا ان شاء الله. اليوم ليس يوما للسكوت، انه يوم النشاط، وعلى كل واحد في اي منصب كان ان لا يسكت،  فتلك الأصوات التي تنطلق",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "35"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "دراسة نفسية الناس فرأوا ان ابناء الشرق سيما  المسلمين وبالاخص الشيعة يولون علماء الدين احتراما خاصا ويتبعونهم. فماذا يفعلون حتى  يفصلوا علماء الدين عن الناس؟ وضعوا خطة تقضي باسقاط هيبة علماء الدين <b>امام<\/b> الناس. أنتم لم تكونوا في عهد رضا شاه، اكثركم لم يكن موجودا، أما أنا فقد ادركت ذلك العهد ومن  هم في عمري الآن يتذكرون ذلك العهد ويتذكرون كيف انهم وطبقا لخطة وضعت في الخارج  حاصروا عالم الدين الى حد",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "43"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "الى اقامة حكومة  دائمة. وسيقوم المجلس التأسيسي بطرح قضية “الجمهورية الإسلامية” للاستفتاء الشعبي… رغم  اني أعتقد بعدم وجود حاجة لهذا الاستفتاء وان الجماهير صوتت لصالح الجمهورية الاسلامية في  اكثر من مناسبة، ولكن ولأجل قطع السبيل <b>امام<\/b> كافة الذرائع وكي يتم احصاء آراء الجماهير  فاننا يجب ان نقوم بهذا الأمر بشكل حر حتى يعرف الناس ويعرف العالم اجمع رأي الجماهير  الحر ولمن يعطى هذا الرأي ولاي نظام. ",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "52"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "لسنة الأولياء، ومخالف للعقل الانساني،  ومخالف لحقوق الانسان، ومخالف للقوانين السائدة في العالم. وتلك الحكومات لم تقم بعمل حقيقي مطلقا. وينبغي - ان شاء الله - ان تأتي حكومة تعتبر  نفسها مسؤولة <b>امام<\/b> الشعب، ينبغي ان نؤسس مجلسا ينبثق من الشعب. لم يكن لدينا خلال  الخمسين عاما الماضية مجلسا يمثلنا نحن، فالمجالس النيابية كانت دوما معينة أما من قبل  الاجانب - الذين كانوا يعينون النواب وهو ما ذكره محمد رضا",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "68"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": " بالطريقة التي اشرنا اليها ودعمهم “للحكومة المؤقتة”. وعلى من يزعم بانه تابع  للشعب، اذا كان صادقا فعليه ان ينضم الى هذا الشعب. ان على الجيش ان ينضم الى الشعب،  فالجيش من الشعب والشعب من الجيش ونحن نعلن دعمنا للجيش، ان علماء الدين يعلنون  دعمهم للجيش، وعلى الجميع في المقابل ان يقف الى صف المسلمين كما هو حال سائر المسلمين، وان  يكون جيشنا جيشا لإمام العصر “سلام الله عليه” لا جيشا لاعداء إمام العصر. يا أبناء الجيش المحترمين! أنتم مسلمون، أنتم اتباع الرسول والقرآن، أنتم اتباع رسول  الإسلام وإمام العصر “سلام الله عليه” وعليكم ان تكونوا صوتا واحدا وان تنضموا الى صفوف  المسلمين، نحن جميعا صوت واحد مع جميع المسلمين، نحن صوت واحد مع كافة ابناء الشعب. اعلان الوحدة والتضامن أننا نعلن عن تضامننا مع كافة الاقليات الدينية، ونعلن عن تآخينا مع الأخوة من ابناء  السنة، فاعداء الإسلام هم الذين يريدون ايقاع الفرقة والخلاف بيننا وبين اخواننا، ان اعداء  الإسلام أو المغفلين - وهم منهم - هم الذين يريدون ايقاع الفرقة في هذا الوقت بين الجانبين. اننا  نعلن وحدة كلمة المسلمين، ولو توحدت كلمة المسلمين لما امكن للأجانب ان يتسلطوا عليهم،  فالفرقة بين المسلمين هي التي ادت الى هيمنة الأجانب علينا. ان الفرقة بين المسلمين كانت منذ  البدء على ايدي اشخاص جاهلين ولازلنا نعاني منهم حتى الان. يجب على جميع المسلمين ان يتحدوا، فالظرف حساس للغاية ونحن نقف بين الحياة والموت.  ان بلادنا الآن اما ان تبقى الى الأبد تحت نير الاستعمار والاستبداد أو تتمكن من انقاذ نفسها. ولو  انكم لم تحفظوا وحدة كلمتكم فانكم ستبقون في هذا البلاء الى الأبد. التآخي بين السنة والشيعة أيها السادة! اننا مسؤولون جميعاً، وان علماء الدين يقفون في الطليعة، وعليهم ان ينتشروا في  البلاد وفي الاماكن النائية، عليهم ان يذهبوا الى القرى والقصبات وان ينقلوا هذه الأمور التي  نطرحها، فمن الممكن ان يستغفل الناس في المناطق النائية وان تنتشر السموم هناك. فهؤلاء لا  يتمكنون من بث الأكاذيب في طهران أو سائر المراكز الهامة في البلاد، لكنهم يبثون في القرى  والقصبات البعيدة بعض الشائعات الكاذبة بشكل خفي، كأن يقولوا مثلا بانه يجب القضاء على  الاقليات الدينية تحت ظل الحكومة الإسلامية! وهذا مخالف للإسلام، فالاسلام يحترم الاقليات  الدينية، ان الاسلام يعتبر الاقليات الدينية الموجودة في بلادنا فئات محترمة. كذلك لا يفرق  ",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "71"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "ايدنا  بايدي بعض واذا تعاضدنا فسنتمكن ان شاء الله من اخراج هذه البلاد من حالة الاضطراب التي  تعيشها، وكما رأيتم فانكم قد تعاضدتم وتمكنتم بحمد الله من التغلب على القوى العظمى. تراجع القوى العظمى <b>امام<\/b> قبضات الجماهير تعلمون بان هذا الرجل (الشاه) الذي كان يتمتع بقدرة عالية مستندا الى جيشه، ولم يكن  وحيدا انما وقفت خلفه جميع القوى الكبرى في العالم، الاتحاد السوفيتي، امريكا غاية الأمر ان  بعضهم صرح بذلك",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "73"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "القيام لله فهو نصر، اما اذا كان القيام شيطانيا ومستندا لاهواء النفس والهوى  الشيطاني فهو هزيمة حتى وان كان نصرا في الظاهر. مثال ذلك مواجهة اميرالمؤمنين علي بن  ابي طالب لمعاوية، فقد وقف الجيشان <b>امام<\/b> بعضهما، احدهما كان طاغوتا وممثلاً لجيش  الطاغوت، والآخر جيش الله، ولو ان الامام انتصر وانتصر معه جيشه فهو منتصر، ولو هزم فهو  منتصر ايضا. في صفين يجب القول ان الامام هزم، لانهم ومن خلال المكر والخداع",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "89"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "هة النظام الظالم. اشكر هذه الحشود العظيمة  من ابناء الشعب الايراني التي خرجت في هذا اليوم للتعبير عن دعمها لحكومة المهندس بازركان  التي تمثل الحكومة الشرعية وحكومة إمام العصر سلام الله عليه. على كافة وكالات الانباء ان تعلم بان هذه المسيرات العظيمة في ايران بدءً من العاصمة وانتهاءً  باقصى المدن والقصبات، انما كانت لدعم هذه الحكومة، وهي كافية للاجابة على تخرصات  البعض من أن الشعب يجب ان يقول كلمته.. ما هي اللغة التي ينبغي للشعب ان يتحدث بها الشعب  يصرخ! الشعب يهتف بهذه الامور منذ وقت طويل، منذ سنين. وهو يعلن اليوم ايضا عبر  صرخاته وشعاراته، دعمه للحكومة الإسلامية وانزجاره من المعارضين لها. اتمام الحجة على حكومة الشاه الاخيرة ان من المناسب لاولئك المعارضين ان يكفوا عن اخطائهم هذه، فالفرصة ماتزال متاحة  لعودتهم الى احضان الشعب، والشعب سيقبل توبتهم، فلا يصروا على معاندة هذا الشعب،  وليتوقفوا عن الحاق الأذى به. لقد ابلغوني الآن عن وقوع حوادث قتل في كركان وبعض المناطق الاخرى. لماذا تقع مثل هذه  الأعمال؟ من هم هؤلاء الاشقياء الذين كان محمد رضا يعيش بحمياتهم وتعيشون انتم الآن  بحمايتهم؟ من أي منظمة هم؟ لماذا تتصرفون مع الشعب بهذه الطريقة؟ لماذا تسفكون هذا القدر  من الدماء؟ لماذا تعاندون الناس بهذه الطريقة؟ اننا نريد لهذه البلاد ان تعيش بهدوء، اننا نريد  صلاح الشعب بأسره، اننا نريد ان يكون لنا جيشا مستقلا، نريد له ان لا يكون مرتبطا بالغير  وان يكون منبثقاً من الشعب. ان على الجيش ان يعود الى احضان الشعب كما عاد الكثيرون وكما عادت العديد من  المجموعات فاستقبلناها بالأحضان، عودوا انتم ايضا، ليعد كافة العاملين في اجهزة الحكومة الى  احضان الشعب. ",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "92"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "بحمد لله بات  بمقدورنا - في ظل هذه الثورة - عقد مثل هذه اللقاءات كي تطرحوا مشاكلكم ونتحدث نحن عما  يقلقنا. طريق الاعمار الطويلة تعلمون باننا، وبعد ان يتحقق لنا ان شاء الله اقامة حكومة ثورية، سنكون <b>امام<\/b> طريق طويلة  لاعادة اعمار الخراب الذي حل بهذه البلاد خلال الخمسة والخمسين عاما الماضية تحت شعار  “الحضارة العظيمة” و”الحرية”. لقد ذهبوا وفروا بعد ان بددوا ثروات البلاد أو نهبوها، وورثنا نحن عنهم بلدا",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "96"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "حوادث مشابهة في بعض المدن. العملاء في قناع الوطنية ان هؤلاء - اعني عملاء الاجانب - يظهرون كل يوم بمظهر جديد وقناع جديد، فمن  المحتمل انهم اعدوا عملاء لهم خلال عشرين عاما او ثلاثين عاما واظهروهم <b>امام<\/b> الناس على  انهم وطنيون، في حين يجهل الشعب الدور المطلوب منهم! وحينما تظهر وجوههم الحقيقية  سيدرك الشعب دورهم. يظهر احدهم على مدى ثلاثين عاما، أو عشرين عاما بنقاب الوطنية  والدين لأنهم يعدونه لوقت آخر",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "98"
  }, {
    "BookAuthor": "",
    "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
    "BookHasPDF": false,
    "BookID": 138,
    "BookPreviewText": "يا من تحملتم من العناء الكثير من أجل هذا الشعب وتعبتم في سبيله.  وأنا اعلم ما هو شعور من يكون منكم بالقرب من احد المظلومين المعذبين المجروحين وهو ينازع  بين الموت والحياة، اعلم ان اغلبهم قد ماتوا <b>امام<\/b> اعينكم واعلم ما تركه ذلك من اثر عليكم.  اسأل الله ان يثيبكم، وان يمن عليكم بالسلامة. لتتظافر جهودكم، اتحدوا في هذه الثورة، فاذا تعرضت هذه الثورة للهزيمة فستهزمون الى  الأبد. كونوا معا ولتتظافر جهودكم",
    "BookPublishYear": "",
    "BookPublisher": null,
    "BookSubject": "صحیفه امام - عربی",
    "BookTitle": "صحیفه عربی- جلد ششم",
    "PageNumberInBook": "99"
  }],
  "SearchPhrase": "امام",
  "TotalFoundEntries": 21,
  "TotalPages": 1
};