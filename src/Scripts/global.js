﻿(function (global, _) {
  _.version = '1.1.0';

  _.wsHost = 'http://94.232.172.91';

  var remoteEnabled = false;

  _.getUrl = function (url) {
    if (remoteEnabled) {
      ///usr/bin/open -a "/Applications/Google Chrome.app" --args --disable-web-security
      if (url.indexOf('/WebServices/') === 0 ||
        url.indexOf('/RTFHandler.ashx') === 0 ||
        url.indexOf('/MediaFiles/') === 0) {
        return _.wsHost + url;
      }
      if (url.indexOf('WebServices/') === 0 ||
        url.indexOf('RTFHandler.ashx') === 0 ||
        url.indexOf('MediaFiles/') === 0) {
        return _.wsHost + '/' + url;
      }
    }

    return url;
  };

  _.$vid = function $vid() {
    if (!$vid.el) {
      $vid.el = $('video');
    }
    return $vid.el;
  };

  _.vid = function vid() {
    if (!vid.el) {
      vid.el = _.$vid().get(0);
    }
    return vid.el;
  };

  _.download = function (config) {
    var form,
      removeNode = _.download.removeNode,
      frameId = Ext.id(),

      iframe = Ext.core.DomHelper.append(document.body, {
        id: frameId,
        name: frameId,
        style: 'display:none',
        tag: 'iframe'
      }),

      inputs = paramsToInputs(config.params);

    // If the download succeeds the load event won't fire but it can in the failure case. We avoid using Ext.Element on
    // the iframe element as that could cause a leak. Similarly, the load handler is registered in such a way as to
    // avoid a closure.
    iframe.onload = _.download.onload;

    form = Ext.DomHelper.append(document.body, {
      action: config.url,
      cn: inputs,
      method: config.method || 'GET',
      tag: 'form',
      target: frameId
    });

    form.submit();

    removeNode(form);

    // Can't remove the iframe immediately or the download won't happen, so wait for 10 minutes
    Ext.defer(removeNode, 1000 * 60 * 10, null, [iframe]);

    function paramsToInputs(params) {
      var inputs = [];

      function fn(key) {
        return function (value) {
          inputs.push(createInput(key, value));
        };
      }

      for (var key in params) {
        var values = [].concat(params[key]);

        // Ext.each(values, function (value) {
        //   inputs.push(createInput(key, value));
        // });

        Ext.each(values, fn(key));
      }

      return inputs;
    }

    function createInput(key, value) {
      return {
        name: Ext.htmlEncode(key),
        tag: 'input',
        type: 'hidden',
        value: Ext.htmlEncode(value)
      };
    }
  };

  // Declared outside the download function to avoid a closure
  _.download.onload = function () {
    // Note we only come into here in the failure case, so you'll need to include your own failure handling
    //var response = this.contentDocument.body.innerHTML;


  };

  // Declared outside the download function to avoid a closure
  _.download.removeNode = function (node) {
    node.onload = null;
    node.parentNode.removeChild(node);
  };

  _.get = function $$get(url, callback, scope) {
    var req = null;
    if (window.XMLHttpRequest) {
      req = new XMLHttpRequest();
    } else {
      req = new ActiveXObject('Microsoft.XMLHTTP');
    }
    req.open('GET', url, true);
    req.onreadystatechange = function () {
      if (req.readyState === 4 && req.status === 200) {
        if (typeof callback === 'function') {
          scope = scope || req;
          callback.call(scope, req.responseText);
        }
      }
    };
    req.send(null);
  };

  _.getJSON = function $$getJSON(url, callback, scope) {
    _.get(url, function (response) {
      var json = null;
      try {
        json = JSON.parse(response);
      } catch (e) {
        console.log('Invalid JSON string!');
      }
      if (typeof callback === 'function') {
        callback.call(this, json);
      }
    }, scope);
  };

  _.getRTFUrl = function (arg) {
    return '/RTFHandler.ashx?userID=' + _.uid() + '&arg=' + arg;
  };

  _.getIndexTabPanel = function () {
    return [{
        title: 'نمایه سازی'
      },
      new Ext.TabPanel({
        id: 'indexTabPanel',
        flex: 1,
        items: _.map(_.indexTabPanels, function (obj) {
          return _.getIndexTab(obj.Key, obj.Value);
        })
      })
    ];
  };

  _.showNode = function (tree, item) {
    var view = tree.getView(),
      el = Ext.fly(view.getNodeByRecord(item));
    el.setVisibilityMode(Ext.Element.DISPLAY);
    el.setVisible(true);
  };

  _.hideNode = function (tree, item) {
    var view = tree.getView(),
      el = Ext.fly(view.getNodeByRecord(item));
    el.setVisibilityMode(Ext.Element.DISPLAY);
    el.setVisible(false);
  };

  _.getNodeName = function (tree, item) {
    var view = tree.getView(),
      el = Ext.fly(view.getNodeByRecord(item)),
      span = el.dom.querySelector('span');

    return span.innerText;
  };

  _.renameNode = function (tree, item, name) {
    var view = tree.getView(),
      el = Ext.fly(view.getNodeByRecord(item)),
      span = el.dom.querySelector('span');

    span.innerText = name;
  };

  _.openResult = function (el) {
    if (!el) {
      console.error('[_.openResult][Invalid parameter]');
      return;
    }
    var parentNode = el.parentNode,
      pageNumber,
      bookId;

    if (!parentNode) {
      console.error('[_.openResult][Invalid parameter]');
      return;
    }

    bookId = parentNode.getAttribute('id');
    pageNumber = parentNode.getAttribute('pageNumber');

    bookId = Number(bookId);
    pageNumber = Number(pageNumber);

    if (!bookId || !pageNumber) {
      console.error('[_.openResult][Invalid bookId or pageNumber in attributes]');
      return;
    }

    _.loadBook(bookId, pageNumber);
  };
  _.openIndexResult = function (el) {
    if (!el) {
      console.error('[_.openIndexResult][Invalid parameter]');
      return;
    }
    var parentNode = el.parentNode,
      pageNumber,
      bookId;

    if (!parentNode) {
      console.error('[_.openIndexResult][Invalid parameter]');
      return;
    }

    var documnetID = el.parentNode.getAttribute('ownerdocumentid');
    var indexId = el.parentNode.getAttribute('indexid');
    indexId = Number.parseInt(indexId, 10);

    if (documnetID !== undefined && !Number.isNaN(indexId)) {
      var obj = _.getDocumentInfo(documnetID);
      if (obj !== undefined) {
        bookId = obj.bookId;
        pageNumber = obj.pageNumber;



        bookId = Number(bookId);
        pageNumber = Number(pageNumber);

        if (!bookId || !pageNumber) {
          console.error('[_.openIndexResult][Invalid bookId or pageNumber in attributes]');
          return;
        }

        _.loadIndex(indexId);

        //_.loadBook(bookId, pageNumber, indexId);
      } else {
        console.error('[_.openIndexResult][Invalid obj in attributes]');
        return;


      }
    } else {

      console.error('[_.openIndexResult][Invalid bookId or pageNumber in attributes]');
      return;

    }
  };

  _.alert = function alert(messsage) {
    if (window.Ext !== undefined &&
      Ext.Msg !== undefined &&
      typeof Ext.Msg.alert === 'function') {
      Ext.Msg.alert(
        'Warning',
        messsage
      );
    } else {
      alert(messsage);
    }
  };

  _.confirm = function (msg, fn) {
    Ext.Msg.confirm('Warning', msg, fn);
  };

  _.getMsg = function $getMsg() {
    var app = null;
    if ((global.dl || !Ext.isFunction(global.dl.getApplication) || !(app = global.dl.getApplication()) || !app.bundle) === undefined) {
      return (arguments[0] || 'NONE');
    }
    app = global.dl.getApplication();
    return app.bundle.getMsg.apply(app.bundle, arguments);
  };

  _.addToolbar = function (tab) {

    if (tab === 0)
      return {

        icon: 'Scripts/resources/ext-theme-classic/images/dd/add.png', // icons can also be specified inline
        cls: 'add-new-group',
        tooltip: 'اضافه کردن گره جدید',
        id: 'addNewNode_' + tab
      };

  };

  _.getIndexTabOld = function (tabId, title) {
    return {
      flex: 1,
      xtype: 'tabpanel',
      items: [{
        title: title,
        id: 'indextab_' + tabId,
        layout: {
          type: 'vbox',
          align: 'stretch'
        },
        flex: 1,
        minWidth: 100,
        height: 200,

        listeners: {
          itemcontextmenu: function () {
            //params: grid, record, item, index, event
            //var x = event.browserEvent.clientX,
            //  y = event.browserEvent.clientY;
            //FIXME: amin: So what??
          }
        },
        tbar: [
          _.addToolbar(tabId), {

            icon: 'Scripts/resources/ext-theme-classic/images/dd/checked.png', // icons can also be specified inline
            cls: 'checked-all',
            tooltip: 'انتخاب همه'
          }, {

            icon: 'Scripts/resources/ext-theme-classic/images/dd/unchecked.png', // icons can also be specified inline
            cls: 'unchecked-all',
            tooltip: 'انتخاب هبچکدام'
          }, {

            icon: 'Scripts/resources/ext-theme-classic/images/dd/filter.png', // icons can also be specified inline
            cls: 'filter',
            tooltip: 'فبلتر'
          }, '-', {

            icon: 'Scripts/resources/ext-theme-classic/images/dd/publish.png',
            menu: {
              xtype: 'menu',
              items: [{
                text: 'انتشار نمابه خود',
                icon: 'Scripts/resources/ext-theme-classic/images/dd/publish.png'
              }, {
                text: 'انتشار ساختار خود',
                icon: 'Scripts/resources/ext-theme-classic/images/dd/publish.png'
              }, {
                text: 'انتشار ساختارهای  منتشر شده',
                icon: 'Scripts/resources/ext-theme-classic/images/dd/publish.png'
              }]
            }
          }, '-', {

            icon: 'Scripts/resources/ext-theme-classic/images/dd/document_export.png',
            tooltip: 'تهیه خروجی',
            menu: {
              xtype: 'menu',
              id: 'exportMenu_' + tabId,
              items: [{
                text: 'خروجی  اکسل',
                type: 'IndexExcel'
                //    , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
              }, {
                text: 'خروجی اکسس',
                type: 'IndexAccess'
                //      ,icon:'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
              }, {
                text: 'خروجی ورد',
                type: 'IndexWord'
              }]
            }
          }, {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/document_import.png',
            tooltip: 'تهیه ورودی',
            menu: {
              xtype: 'menu',

              items: [{
                text: 'ورود اطلاعات از اکسل'
                //   , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
              }, {
                text: 'ورود اطلاعات از اکسس'
                //   , icon: 'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
              }]
            }
          }
        ],
        items: [
          _.getIndexBookTree(tabId), {
            xtype: 'splitter',
            collapsible: true,

            collapseTarget: 'next'
          }, {
            flex: 1,

            title: 'نتایج جستجو',
            layout: 'fit',
            items: [Ext.create('Ext.grid.Panel', {
              store: Ext.create('Ext.data.ArrayStore', {
                id: 'indexStore_' + 0,
                fields: [{
                  name: 'Path',
                  type: 'string'
                }, {
                  name: 'IsIndex',
                  type: 'string'
                }, {
                  name: 'IndexOrNodeID',
                  type: 'array'
                }]
              }),
              columns: [{
                flex: 1,
                dataIndex: 'Path'
              }],
              listeners: {

                itemclick: function () {

                  var treePanel = this.getView().up('tabpanel').getActiveTab().down('treepanel');
                  //Ext's solution is to call get instead of reading raw data
                  //like: items[0].get('IndexOrNodeID')
                  var nodeId = this.getSelectionModel().selected.items[0].raw.IndexOrNodeID;

                  var rn = treePanel.getRootNode();
                  rn.findChildBy(function (child) {
                    //params: child, treePanel
                    var id = child.data.id;
                    if (id === nodeId) {
                      console.warn('selecting child', child);
                      child.getOwnerTree().getSelectionModel().select(child, true);
                    }
                  });
                }
              }
            })]
          }
        ]
      }]
    };
  };
  _.getIndexTab = function $$getIndexTab(tabId, title) {
    var indexTab = {
      title: title,
      id: 'indextab_' + tabId,
      layout: {
        type: 'vbox',
        align: 'stretch'
      },
      flex: 1,
      minWidth: 100,
      height: 200,

      listeners: {
        itemcontextmenu: function () {
          //params:grid, record, item, index, event
          //var x = event.browserEvent.clientX,
          //  y = event.browserEvent.clientY;
          //FIXME: amin: so what?
        }
      },
      tbar: [
        _.addToolbar(tabId), {

          icon: 'Scripts/resources/ext-theme-classic/images/dd/checked.png', // icons can also be specified inline
          cls: 'checked-all',
          tooltip: 'انتخاب همه'
        }, {

          icon: 'Scripts/resources/ext-theme-classic/images/dd/unchecked.png', // icons can also be specified inline
          cls: 'unchecked-all',
          tooltip: 'انتخاب هبچکدام'
        }, {

          icon: 'Scripts/resources/ext-theme-classic/images/dd/filter.png', // icons can also be specified inline
          cls: 'filter',
          tooltip: 'فبلتر'
        }, '-', {

          icon: 'Scripts/resources/ext-theme-classic/images/dd/publish.png',
          menu: {
            xtype: 'menu',
            items: [{
              text: 'انتشار نمابه خود',
              icon: 'Scripts/resources/ext-theme-classic/images/dd/publish.png'
            }, {
              text: 'انتشار ساختار خود',
              icon: 'Scripts/resources/ext-theme-classic/images/dd/publish.png'
            }, {
              text: 'انتشار ساختارهای  منتشر شده',
              icon: 'Scripts/resources/ext-theme-classic/images/dd/publish.png'
            }]
          }
        }, '-', {

          icon: 'Scripts/resources/ext-theme-classic/images/dd/document_export.png',
          tooltip: 'تهیه خروجی',
          menu: {
            xtype: 'menu',
            id: 'exportMenu_' + tabId,
            items: [{
              text: 'خروجی  اکسل',
              type: 'IndexExcel',
              icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
            }, {
              text: 'خروجی اکسس',
              type: 'IndexAccess',
              icon: 'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
            }, {
              text: 'خروجی ورد',
              type: 'IndexWord'
            }]
          }
        }, {
          icon: 'Scripts/resources/ext-theme-classic/images/dd/document_import.png',
          tooltip: 'تهیه ورودی',
          menu: {
            xtype: 'menu',

            items: [{
              text: 'ورود اطلاعات از اکسل',
              icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
            }, {
              text: 'ورود اطلاعات از اکسس',
              icon: 'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
            }]
          }
        }
      ],
      items: [
        _.getIndexBookTree(tabId), {
          xtype: 'splitter',
          collapsible: true,

          collapseTarget: 'next'
        }, {
          flex: 1,

          title: 'نتایج جستجو',
          layout: 'fit',
          items: [Ext.create('Ext.grid.Panel', {
            store: Ext.create('Ext.data.ArrayStore', {
              id: 'indexStore_' + tabId,
              fields: [{
                name: 'Path',
                type: 'string'
              }, {
                name: 'IsIndex',
                type: 'string'
              }, {
                name: 'IndexOrNodeID',
                type: 'array'
              }]
            }),
            columns: [{
              flex: 1,
              dataIndex: 'Path'
            }],
            listeners: {

              itemclick: function () {

                var treePanel = this.getView().up('tabpanel').getActiveTab().down('treepanel');
                var nodeId = this.getSelectionModel().selected.items[0].raw.IndexOrNodeID;
                var rn = treePanel.getRootNode();
                rn.findChildBy(function (child) {
                  //params: child, treePanel
                  var id = child.data.id;
                  if (id === nodeId) {
                    console.warn('selecting child', child);
                    child.getOwnerTree().getSelectionModel().select(child, true);
                  }
                });
              }
            }
          })]
        }
      ]
    };

    return indexTab;
  };

  if (remoteEnabled) {
    var oldOpen = XMLHttpRequest.prototype.open;
    XMLHttpRequest.prototype.open = function (method, url) {
      //params: method, url, async
      var args = Array.prototype.slice.call(arguments);
      args[1] = _.getUrl(url);
      return oldOpen.apply(this, args);
    };
  }

}(window, _, jQuery));