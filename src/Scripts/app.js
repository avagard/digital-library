﻿//Ext.Loader.setConfig({
//  disableCaching: false
//});
_.loadApplication = function () {
  Ext.application({

    name: 'dl',
    appFolder: '/Scripts/app',

    requires: [
      'Ext.i18n.Bundle'
    ],

    controllers: [
      'Main'
    ],
    bundle: {
      bundle: 'Application',
      lang: 'fa-IR',
      path: '/Scripts/resources',
      noCache: true
    },
    views: [
      'Viewport',
      'BookTree',
      'UserBookTree',
      'BookSearchDataView',
      'IndexSearchGrid',
      // 'IndexBookTree',
      'PictureSearchView',
      'HighlightedGrid',
      'BookmarkedGrid',
      'CommentedGrid',
      'AvagardMediaView',
      'DocumentMediaView'
    ],
    stores: [
      'BookTreeStore',
      'UserBookTreeStore',
      'IndexBookTreeStore',
      'HighlightStore',
      'CommentedStore',
      'BookmarkedStore',
      'BookSearchStore',
      'BookSearchDataViewStore',
      'AvagardMediaViewStore',
      'DocumentMediaViewStore',
      'IndexSearchDataViewStore'
    ],

    launch: function (indexTabPanels) {
      var mainView = Ext.create('dl.view.Viewport', {
        renderTo: Ext.getBody()
      });

      mainView.show();
    }

  });
};

_.ws.getIndexTabs().then(function(indexTabPanels){
  _.indexTabPanels = indexTabPanels;
  _.loadApplication();
});