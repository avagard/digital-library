Ext.define('dl.model.IndexSearchResultModel', {
  extend: 'Ext.data.Model',
  fields: ['SearchPhrase', 'TotalFoundEntries', 'TotalPages', 'FoundItems'],
  hasMany: [{

    model: 'dl.model.IndexFoundItemsModel',
    associationKey: 'FoundItems',
    name: 'FoundItems'
  }]
});