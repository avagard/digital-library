﻿Ext.define('dl.model.MediaViewModel', {
    extend: 'Ext.data.Model',
    fields: ['MediaID', 'Title','Files'],
    hasMany: [{
        model: 'dl.model.FileModel'
        , associationKey: 'Files'
        , name: 'Files'
    }]
});