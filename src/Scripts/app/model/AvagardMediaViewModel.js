﻿Ext.define('dl.model.AvagardMediaViewModel', {
  extend: 'Ext.data.Model',
  fields: ['MediaID', 'Title', 'Files', 'bookId', 'pageNumber'],
  hasMany: [{
    model: 'dl.model.FileModel',
    associationKey: 'Files',
    name: 'Files'
  }]
});
