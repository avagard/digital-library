﻿Ext.define('dl.model.FileModel', {
    extend: 'Ext.data.Model',
    fields: ['FileID', 'FileName', 'URL']
});
