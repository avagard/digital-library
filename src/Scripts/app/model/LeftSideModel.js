﻿Ext.define('dl.model.LeftSideModel', {
    extend: 'Ext.data.Model',
    fields: ['BookTile', 'DocumentTitle', 'ID', 'BookID', 'Title', 'Type']
});
