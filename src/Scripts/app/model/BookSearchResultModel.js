﻿Ext.define('dl.model.BookSearchResultModel', {
    extend: 'Ext.data.Model',
    fields: ['SearchPhrase', 'TotalFoundEntries', 'TotalPages','FoundItems'],
    hasMany: [{
        
         model: 'dl.model.FoundItemsModel'
        , associationKey: 'FoundItems'
        , name: 'FoundItems'
    }]
});
