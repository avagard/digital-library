﻿Ext.define('dl.model.TextTool', {
    extend: 'Ext.data.Model',
    fields: ['ID', 'BookTile', 'DocumentTitle', 'Title', 'Type']
});
