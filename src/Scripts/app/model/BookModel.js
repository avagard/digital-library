﻿Ext.define('dl.model.BookModel', {
    extend: 'Ext.data.Model',
    fields: ['id', 'name', 'profile_image_url']
});
