﻿Ext.define('dl.model.FoundItemsModel', {
    extend: 'Ext.data.Model',
    fields: ['BookAuthor', 'BookCoverImagePath', 'BookHasPDF', 'BookID', 'BookPreviewText', 'BookPublishYear', 'BookSubject', 'BookTitle', 'PageNumberInBook'],
    });
