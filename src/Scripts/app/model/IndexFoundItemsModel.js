Ext.define('dl.model.IndexFoundItemsModel', {
  extend: 'Ext.data.Model',
  fields: ['IndexID', 'IndexTextRTF', 'IndexTitle', 'Length', 'OwnerDocumentID', 'Path', 'StartPosition'],
});