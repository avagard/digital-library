﻿Ext.define('dl.store.BookSearchDataViewStore', {
    extend: 'Ext.data.Store',
    model: 'dl.model.BookSearchResultModel',
    autoLoad: false,
    proxy: {
        type: 'rest',
        reader: {
            type: 'json',
            rootProperty: 'list'
        }
    }
    
});
