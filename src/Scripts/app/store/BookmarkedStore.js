﻿Ext.define('dl.store.BookmarkedStore', {
    extend: 'Ext.data.Store',
    model: 'dl.model.LeftSideModel',
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: '/WebServices/DigitalLibraryRESTService.svc/GetBookmarks/' + _.uid(),
        reader: {
            type: 'json',
            rootProperty: 'list'
        }
    }
});
