﻿Ext.define('dl.store.BookSearchStore', {
    extend: 'Ext.data.Store',
    model: 'dl.model.BookSearchResultModel',
    autoLoad: false,
    proxy: {
        type: 'memory',//'rest',
        //url: '/Scripts/app/data/bookSearch.json',//'/WebServices/DigitalLibraryRESTService.svc/SearchBooks/' + _.uid() + '/مقایسه%20فنی%20اقتصادی%20سازه%20های/1/1/1',
        reader: {
            type: 'json',
            rootProperty: 'FoundItems'
        }
    }, listeners: {
        'load': function (store, records, options) {
            if(records.length){
                Ext.getStore('BookSearchDataViewStore').loadData(records[0].raw.FoundItems, false);
            }
        }
    }
});
