Ext.define('dl.store.BookTreeStore', {
    extend: 'Ext.data.TreeStore',
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: '/WebServices/DigitalLibraryRESTService.svc/GetBooksTree/'+_.uid()+'/allbooks',
        reader: {
          
             root: 'List'
        }
    }
});
