Ext.define('dl.store.IndexBookTreeStorePersonal', {
  extend: 'Ext.data.TreeStore',
  autoLoad: false,
  proxy: {
    type: 'rest',
    url: '/WebServices/DigitalLibraryRESTService.svc/GetIndexBooksTree/' + _.uid() + '/2',
    reader: {

      root: 'List'
    }
  }
});
