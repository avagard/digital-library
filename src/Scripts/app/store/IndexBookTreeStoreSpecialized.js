Ext.define('dl.store.IndexBookTreeStoreSpecialized', {
  extend: 'Ext.data.TreeStore',
  autoLoad: false,
  proxy: {
    type: 'rest',
    url: '/WebServices/DigitalLibraryRESTService.svc/GetIndexBooksTree/' + _.uid() + '/0',
    reader: {

      root: 'List'
    }
  }
});
