﻿
Ext.define('dl.store.DocumentMediaViewStore', {
    extend: 'Ext.data.Store',
    model: 'dl.model.AvagardMediaViewModel',
    autoLoad: false,
    proxy: {
        type: 'memory',
        reader: {
            type: 'json'
            //, rootProperty: 'list'
        }
    }
});
