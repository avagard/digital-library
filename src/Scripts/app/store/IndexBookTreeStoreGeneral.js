Ext.define('dl.store.IndexBookTreeStoreGeneral', {
  extend: 'Ext.data.TreeStore',
  autoLoad: false,
  proxy: {
    type: 'rest',
    url: '/WebServices/DigitalLibraryRESTService.svc/GetIndexBooksTree/' + _.uid() + '/1',
    reader: {

      root: 'List'
    }
  }
});
