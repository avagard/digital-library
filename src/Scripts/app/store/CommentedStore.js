﻿Ext.define('dl.store.CommentedStore', {
    extend: 'Ext.data.Store',
    model: 'dl.model.LeftSideModel',
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: '/WebServices/DigitalLibraryRESTService.svc/GetComments/' + _.uid(),
        reader: {
            type: 'json',
            rootProperty: 'list'
        }
    }
});
