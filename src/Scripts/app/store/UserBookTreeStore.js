Ext.define('dl.store.UserBookTreeStore', {
    extend: 'Ext.data.TreeStore',
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: '/WebServices/DigitalLibraryRESTService.svc/GetBooksTree/' + _.uid() + '/userbooks',
        reader: {
          
             root: 'List'
        }
    }
});
