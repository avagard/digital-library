﻿Ext.define('dl.store.AvagardMediaViewStore', {
    extend: 'Ext.data.Store',
    model: 'dl.model.AvagardMediaViewModel',
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: '/WebServices/DigitalLibraryRESTService.svc/GetMedia/1/null/0/50',
        reader: {
            type: 'json',
            rootProperty: 'list'
        }
    }
});
