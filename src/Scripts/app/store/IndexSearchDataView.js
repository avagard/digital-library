Ext.define('dl.store.IndexSearchDataViewStore', {
  extend: 'Ext.data.Store',
  model: 'dl.model.IndexSearchResultModel',
  autoLoad: false,
  proxy: {
    type: 'rest',
    reader: {
      type: 'json',
      rootProperty: 'list'
    }
  }

});