Ext.define('dl.store.IndexBookTreeStore', {
  extend: 'Ext.data.TreeStore',
  autoLoad: false,
  proxy: {
    type: 'rest',
    url: '/WebServices/DigitalLibraryRESTService.svc/GetIndexBooksTree/' + _.uid() + '/0',
    reader: {

      root: 'List'
    }
  }
});
