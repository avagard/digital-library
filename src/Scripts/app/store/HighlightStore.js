﻿Ext.define('dl.store.HighlightStore', {
    extend: 'Ext.data.Store',
    model: 'dl.model.TextTool',
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: '/WebServices/DigitalLibraryRESTService.svc/GetHighlights/' + _.uid(),
        reader: {
            type: 'json',
            rootProperty: 'list'
        }
    }
});
