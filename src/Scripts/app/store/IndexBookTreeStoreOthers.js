Ext.define('dl.store.IndexBookTreeStoreOthers', {
  extend: 'Ext.data.TreeStore',
  autoLoad: false,
  proxy: {
    type: 'rest',
    url: '/WebServices/DigitalLibraryRESTService.svc/GetIndexBooksTree/' + _.uid() + '/4',
    reader: {

      root: 'List'
    }
  }
});
