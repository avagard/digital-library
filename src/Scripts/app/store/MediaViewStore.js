﻿Ext.define('dl.store.MediaViewStore', {
    extend: 'Ext.data.Store',
    model: 'dl.model.MediaViewModel',
    autoLoad: true,
    proxy: {
        type: 'rest',
        url: '/WebServices/DigitalLibraryRESTService.svc/GetMedia/1/null/0/50',
        reader: {
            type: 'json',
            rootProperty: 'list'
        }
    }
});
