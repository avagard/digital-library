Ext.define('dl.controller.Main', {
  extend: 'Ext.app.Controller',
  requires: [
    'dl.view.*',
    'Ext.window.Window'
  ],
  views: [
    'dl.view.BookTree'
  ],
  stores: [

  ],
  init: function () {
    this.control({
      'viewport toolbar #exitBtn': {
        click: this.onExit
      },
      'viewport toolbar button[cls=searchEnv]': {
        click: this.searchEnv
      },
      'codePreview tool[type=maximize]': {
        click: 'onMaximizeClick'
      },
      'contentPanel': {
        resize: 'centerContent'
      },
      'tool[regionTool]': {
        click: 'onSetRegion'
      },
      'viewport #btnSearch': {
        click: this.doSearch
      },
      'viewport #searchCriteria': {
        specialkey: function (inpt, e) {
          console.log('SPECIALKEY', (e.keyCode == e.ENTER));
          if (e.keyCode == e.ENTER) {
            this.doSearch(inpt);
          }
        }
      },
      'viewport toolbar #btnAddToUserLibrary': {
        click: this.addToUserLibrary
      },
      'viewport toolbar #btnbookProperties': {
        click: this.getBookProperties
      },
      'viewport toolbar #btnViewPdf': {
        click: this.getPDFurl
      },
      'viewport  #addCategory': {
        click: this.addCategory

      },
      'viewport  #deleteCategory': {
        click: this.deleteCategory
      },
      'viewport  #renameCategory': {
        click: this.renameCategory
      },
      'viewport  #addNewNode': {
        click: this.AddNewNode
      },
      'viewport  #addNewNode_0': {
        click: this.indexTabAddItem
      },
      'viewport  #deleteHighlightNode': {
        click: this.deleteHighlightNode
      },
      'viewport  #deleteBookmarkedNode': {
        click: this.deleteBookmarkedNode
      },
      'viewport  #deleteCommentedNode': {
        click: this.deleteCommentedNode
      },
      'viewport  #filter_commentedGrid': {
        click: this.filter_Grid
      },
      'viewport #filter_highlightedGrid': {
        click: this.filter_Grid
      },
      'viewport #filter_bookmarkGrid': {
        click: this.filter_Grid
      },
      'viewport  #exportDocumentLibrary': {
        click: this.exportDocumentLibrary
      },
      'viewport  #exportMenu_0': {
        click: this.exportDocumentLibrary
      },
      'viewport  #exportMenu_1': {
        click: this.exportDocumentLibrary
      },
      'viewport  #exportMenu_2': {
        click: this.exportDocumentLibrary
      },
      'viewport  #exportMenu_4': {
        click: this.exportDocumentLibrary
      },
      'viewport  pagingtoolbar': {
        move: this.onPagingMove
      }
    });
  },

  onPagingMove: function (toolbar) {
    var pageNumber = _.loadPaging(toolbar);
    if (pageNumber) {
      _.loadBook(_.currentBookId, pageNumber);
    }
  },

  onExit: function () {
    Ext.MessageBox.confirm('اخطار', 'آیا قصد خروج دارید؟', _.logout);
  },
  searchEnv: function () {
    _.alert('search enviroment');
  },
  doSearch: function (btn) {
    var view = btn.up("#searchCenterTab"),
      autoCorrect = view.down('#autoCorrect-check').getValue(),
      mediaSearch = view.down('#mediaSearch-check').getValue(),
      findSynonyms = view.down('#findSynonyms-check').getValue(),
      searchInPathIndex = view.down('#searchInPathIndex').getValue(),
      searchInTextIndex = view.down('#searchInTextIndex').getValue(),
      isLeafValueableRg = view.down('#isLeafValueableRadiogroup').getValue(),
      codeText = view.down('#codeTextbox').getValue(),
      elementIDText = view.down('#elementIDTextbox').getValue(),
      subjectText = view.down('#subjectTextbox').getValue(),
      keyWordsText = view.down('#keyWordsTextbox').getValue(),
      audiencesText = view.down('#audiencesTextbox').getValue(),
      activeTab = view.up('tabpanel').down('tabpanel').getActiveTab(),
      nemidonamChiVariable = '',
      methodName = '',
      activeTabType = activeTab.type,
      searchOptionsStr = '',
      myStore,
      searchOptions = {};

    searchOptions.autoCorrect = autoCorrect ? 'True' : 'False';

    if (activeTabType == 'book') {
      searchOptions.mediaSearch = mediaSearch ? 'True' : 'False';
      searchOptions.findSynonyms = findSynonyms ? 'True' : 'False';


      methodName = 'SearchBooks'
      view.down("#bookSearchDataViewId").show();
      nemidonamChiVariable = '/1/30';
      searchOptionsStr = JSON.stringify(searchOptions);
      myStore = Ext.getStore('BookSearchDataViewStore');
    } else if (activeTabType == 'index') {
      searchOptions.searchInPath = searchInPathIndex ? 'True' : 'False';
      searchOptions.searchInText = searchInTextIndex ? 'True' : 'False';
      /*searchOptions.isLeafValueable = (isLeafValueableRg.parent)*/
      methodName = 'SearchIndexes'
      view.down("#indexSearchGrid").show();
      nemidonamChiVariable = '/1/30';
      searchOptionsStr = JSON.stringify(searchOptions);
      myStore = Ext.getStore('IndexSearchDataViewStore');

    } else if (activeTabType == 'media') {
      searchOptions.code = codeText;
      searchOptions.elementID = elementIDText;
      searchOptions.subject = subjectText;
      searchOptions.keyWords = keyWordsText;
      searchOptions.audiences = audiencesText;
      methodName = 'SearchImages';
      myStore = view.down("#pictureSearchView").getStore();
      searchOptionsStr = encodeURIComponent(JSON.stringify(searchOptions))
    }
    searchOptions = btoa(searchOptionsStr);

    myStore.load({
      url: '/WebServices/DigitalLibraryRESTService.svc/' + methodName + '/' + _.uid() + '/' + view.down(
        "#searchCriteria").value + nemidonamChiVariable + '/' + searchOptions
    });

  },

  addToUserLibrary: function (btn) {

    var tree = btn.up('tabpanel').getActiveTab().down('treepanel'),
      selectionModel = tree.getSelectionModel(),
      obj = selectionModel.getSelection()[0].raw;

    _.ws.addToUserLibrary(obj['NodeType'], obj['id'], obj['text'], function (params) {
      _.alert('با موفقیت به کتابخانه شما اضافه شد')
    });

  },
  getBookProperties: function (btn) {
    var tree = btn.up('tabpanel').getActiveTab().down('treepanel');
    var selectionModel = tree.getSelectionModel();
    var obj = selectionModel.selected.items[0].raw;


    _.ws.getBookProperties(obj['id'], function (params) {
      _.alert(params);
    });
  },
  getPDFurl: function (btn) {
    var tree = btn.up('tabpanel').getActiveTab().down('treepanel'),
      selectionModel = tree.getSelectionModel(),
      obj = selectionModel.selected.items[0].raw;

    _.ws.getPDFUrl(obj['id'], function (params) {
      _.alert(params);
    })

  },
  addCategory: function (btn) {

    var tree = btn.up('tabpanel').getActiveTab().down('treepanel');
    var root = tree.getRootNode();
    Ext.create('Ext.window.Window', {
      title: 'نام را وارد کنید',
      height: 55,
      mytree: tree,
      width: 210,
      rtl: true,
      resizable: false,
      layout: 'hbox',
      items: [{
          xtype: 'textfield',
          name: 'text',
          width: 150
        },
        Ext.create('Ext.Button', {
          text: 'تایید',
          width: 50,
          listeners: {
            click: function () {
              _.ws.addCategory(this.up('window'), this.up('window')
                .down('textfield').value, function (scope, params) {
                  console.log(params)
                  Ext.getStore('UserBookTreeStore').reload();
                  scope.close();
                });
            }
          }
        })
      ]
    }).show();

  },
  doDelete: function doDelete(tree, item) {
    var methodName = '',
      id = item.get('id'),
      iconCls = item.get('iconCls');



    _.hideNode(tree, item);

    _.ws.deleteCategory(tree, item, iconCls, id, item.get('parentId'), function (item, params) {

      item.remove();
      console.log(params + "UserBookTreeStore reloaded");

    }, function (tree, item, params) {

      _.showNode(tree, item);
      _.alert('server-side failure with status code ' + params);

    });
  },
  deleteCategory: function (btn) {
    var that = this,
      tree = btn.up('tabpanel').getActiveTab().down('treepanel'),
      selectionModel = tree.getSelectionModel(),
      item = selectionModel.selected.items[0],
      obj = item.raw;

    if (obj['leaf'] == true) {
      return;
    }

    _.confirm(_.getMsg('deleteConfirm'), function (btn) {
      if (btn.toLowerCase() === 'yes') {
        that.doDelete(tree, item);
      }
    });

  },
  renameCategory: function (btn) {
    var tree = btn.up('tabpanel').getActiveTab().down('treepanel'),
      selectionModel = tree.getSelectionModel(),
      item = selectionModel.selected.items[0],
      id = item.get('id'),
      iconCls = item.get('iconCls');

    id = id.slice(4);

    Ext.create('Ext.window.Window', {
      title: 'تغییر نام',
      height: 55,
      mytree: tree,
      width: 210,
      rtl: true,
      resizable: false,
      nodeId: id,
      layout: 'hbox',
      items: [{
          xtype: 'textfield',
          name: 'text',
          width: 150,
          value: item.get('text')
        },
        Ext.create('Ext.Button', {
          text: 'تایید',
          width: 50,
          listeners: {
            click: function () {
              var oldName = _.getNodeName(tree, item),
                name = this.up('window').down('textfield').value;

              _.renameNode(tree, item, name);

              _.ws.renameCategory(tree, item, oldName, id, name, function (params) {

              }, function (tree, item, oldName) {
                _.renameNode(tree, item, oldName);
                _.alert('server-side failure with status code ');

              });
              this.up('window').close();
            }
          }
        })
      ]
    }).show();
    if (iconCls == "subject-node") {

    }
  },
  AddNewNode: function (btn) {
    var tree = btn.up('tabpanel').getActiveTab().down('treepanel');
    var root = tree.getRootNode();
    root.appendChild({
      NodeType: 0,
      iconCls: "subject-node",
      id: "",
      leaf: false,
      text: "گره جدید"
    });

    _.ws.addNewNode();
    tree.getView().refresh();
  },
  indexTabAddItem: function (btn) {

    var tree = btn.up('tabpanel').getActiveTab().down('treepanel');
    var root = tree.getRootNode();
    var selectionModel = tree.getSelectionModel();
    var _text = '';
    if (selectionModel.selected.items.length == 0) {
      _text = 'گره جدید';
      root.appendChild({
        NodeType: 0,
        iconCls: "subject-node",
        id: "",
        leaf: false,
        text: _text,
        "checked": false
      });
      _parentId = 'null';
    } else {
      _text = 'پوشه جدید';
      selectionModel.selected.items[0].appendChild({
        NodeType: 0,
        iconCls: "subject-node",
        id: "",
        leaf: false,
        text: _text,
        "checked": false
      });
      _parentId = selectionModel.selected.items[0].data.id;
    }

    _.ws.addnewIndexNode(_text, _parentId, function (params) {

      console.log(params);

      var treepanelView = Ext.ComponentQuery.query('#addNewNode_0')[0].up('tabpanel').getActiveTab().down('treepanel').getView();
      treepanelView.getStore().reload();

    });

  },
  deleteHighlightNode: function (btn) {

    var grid = btn.up('tabpanel').getActiveTab().down('grid'),
      selectionModel = grid.getSelectionModel(),
      obj = selectionModel.selected.items[0].raw;

    _.ws.deleteEmphasize(obj['ID'], function (params) {

      console.log(params);
      var tabPanel = Ext.ComponentQuery.query('#deleteHighlightNode')[0].up('tabpanel').getActiveTab().down('grid').getView();
      tabPanel.getStore().reload();

    })

  },
  deleteBookmarkedNode: function (btn) {

    var grid = btn.up('tabpanel').getActiveTab().down('grid'),
      selectionModel = grid.getSelectionModel(),
      obj = selectionModel.selected.items[0].raw;

    _.ws.deleteBookmarkedNode(obj['ID'], function (params) {

      console.log(response.responseText);
      var tabPanel = Ext.ComponentQuery.query('#deleteBookmarkedNode')[0].up('tabpanel').getActiveTab().down('grid').getView();
      tabPanel.getStore().reload();

    });

  },
  deleteCommentedNode: function (btn) {

    var grid = btn.up('tabpanel').getActiveTab().down('grid'),
      selectionModel = grid.getSelectionModel(),
      obj = selectionModel.selected.items[0].raw;

    _.ws.deleteBookmarkedNode(obj['ID'], function (params) {

      console.log(response.responseText);
      var tabPanel = Ext.ComponentQuery.query('#deleteCommentedNode')[0].up('tabpanel').getActiveTab().down('grid').getView();
      tabPanel.getStore().reload();

    });


  },
  filter_Grid: function (btn, checked) {

    var grid = btn.up('tabpanel').getActiveTab().down('grid');
    var filter = '';
    var store = grid.getStore();
    store.clearFilter();

    if (checked.mode == 0) {

    } else if (checked.mode == 1) {
      filter = 3;
    } else if (checked.mode == 2) {

    }

    store.filter('BookId', filter)
  },
  exportDocumentLibrary: function (menu) {

    var type = arguments[1],
      treePanel = menu.up('tabpanel').down('treepanel'),
      selectionModel = treePanel.getSelectionModel();
    selected = selectionModel.getSelection();
    _.ws.callExport(type, selected);

  }
});