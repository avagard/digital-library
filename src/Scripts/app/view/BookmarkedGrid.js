﻿Ext.define('dl.view.BookmarkedGrid', {
  extend: 'Ext.grid.Panel',
  xtype: 'bookmarkedGrid',
  store: 'BookmarkedStore',

  columns: [{
    text: "عنوان کتاب",
    flex: 1,
    dataIndex: 'BookTile'
  }, {
    text: "صفحه",
    flex: .5,
    dataIndex: 'DocumentTitle'
  }, {
    text: "عنوان",
    flex: 1,
    dataIndex: 'Title'
  }],
  columnLines: true,
  listeners: {
    itemclick: function (grid, record, item, index, event) {
      var
        store = grid.getStore(),
        id = record.get('ID'),
        bookId = record.get('BookID');

      window._currentBookmarkRecord = record;

      _.loadBookmarkedBook(bookId, id, record, store.data);
    },
    afterrender: function(grid){
      _.cacheAllPinpoints(grid.getStore());
    }
  }
});