﻿Ext.define('dl.view.Viewport', {
  extend: 'Ext.container.Viewport',
  alias: 'widget.viewport',
  eastReginTitle: 'کتابخانه',
  requires: [
    'Ext.tab.Panel',
    'Ext.layout.container.Border'
  ],
  rtl: true,
  layout: 'border',

  items: [{
      region: 'north',
      xtype: 'panel',
      height: 70,
      layout: 'fit',
      items: [{
          xtype: 'toolbar',
          id: 'upToolbar',
          height: 70,
          rtl: true,

          // layout: 'fit',
          items: [{
              xtype: 'image',
              width: 291,
              height: 60,
              src: '/Images/logo-60-291.png',
              tooltip: ''
            }, {
              hidden: true,
              text: 'محیط جستجو',
              cls: 'searchEnv'
            }, {
              hidden: true,
              text: 'محیط تنظیمات'
            }, {
              hidden: true,
              text: 'ابزارها'
            }, {
              hidden: true,
              text: 'راهنما'
            }, {
              xtype: 'tbfill'
            }, {
              xtype: 'label',
              text: 'آواگرد ' + _.version
            }, {
              xtype: 'button',
              text: 'خروج',
              id: 'exitBtn'
            }

          ]


        }


      ]

    }, {
      region: 'east',
      id: 'east-region',
      stateful: true,
      stateId: 'mainnav.east',
      split: true,
      collapsible: true,
      width: 200,
      layout: 'fit',
      items: [{
        xtype: 'tabpanel',
        layout: 'fit',
        title: 'رنگی سازی',
        tabPosition: 'bottom',
        items: [{
          title: 'رنگی سازی',
          layout: 'fit',
          tbar: [{
            xtype: 'toolbar',
            width: '100%',
            items: [{
              xtype: 'splitbutton',
              icon: 'Scripts/resources/ext-theme-classic/images/dd/sort_ascend.png',
              tooltip: 'مرتب کردن',
              handler: function (item) {
                var isDescend = (item.icon === 'Scripts/resources/ext-theme-classic/images/dd/sort_descend.png');
                if (isDescend) {
                  item.setIcon('Scripts/resources/ext-theme-classic/images/dd/sort_ascend.png');
                } else {
                  item.setIcon('Scripts/resources/ext-theme-classic/images/dd/sort_descend.png');
                }
              },
              menu: {
                xtype: 'menu',
                items: [{
                  text: 'هیچکدام'
                  //    , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
                }, {
                  text: 'براساس عنوان کتاب'
                  //      ,icon:'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
                }, {
                  text: 'براساس شماره صفحه'
                }, {
                  text: 'براساس علامت'
                }]
              }
            }, {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/filter.png',
              cls: 'filter',
              tooltip: 'فیلتر کردن',
              menu: {
                xtype: 'menu',
                id: 'filter_highlightedGrid',

                items: [{
                  text: 'نمایش همه ی علامت ها',
                  mode: '0'
                  //    , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
                }, {
                  text: 'نمایش علامت های کتاب  جاری',
                  mode: '1'
                  //      ,icon:'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
                }, {
                  text: 'نمایش علامت های صفحه جاری',
                  mode: '2'
                }]
              }
            }, '-', {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/next.png',

              tooltip: 'بعدی',

            }, {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/pre.png',

              tooltip: 'قبلی',

            }, '-', {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/delete.png',

              tooltip: 'حذف',
              id: 'deleteHighlightNode'

            }, {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/rename.png',

              tooltip: 'تغییر نام',

            }]

          }],
          items: [{
            xtype: 'highlightedGrid'
          }]

        }, {
          title: 'علامت گذاری',
          layout: 'fit',
          tbar: [{
            xtype: 'splitbutton',
            icon: 'Scripts/resources/ext-theme-classic/images/dd/sort_ascend.png',
            tooltip: 'مرتب کردن',
            handler: function (item) {
              if (item.icon === 'Scripts/resources/ext-theme-classic/images/dd/sort_descend.png') {
                item.setIcon('Scripts/resources/ext-theme-classic/images/dd/sort_ascend.png');
              } else {
                item.setIcon('Scripts/resources/ext-theme-classic/images/dd/sort_descend.png');
              }
            },
            menu: {
              xtype: 'menu',
              id: 'filter_bookmarkGrid',
              items: [{
                text: 'هیچکدام',
                mode: '0'
                //    , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
              }, {
                text: 'براساس عنوان کتاب',
                mode: '1'
                //      ,icon:'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
              }, {
                text: 'براساس شماره صفحه',
                mode: '2'
              }, {
                text: 'براساس علامت',
                mode: '3'
              }]
            }
          }, {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/filter.png',
            cls: 'filter',
            tooltip: 'فیلتر کردن',
            menu: {
              xtype: 'menu',
              items: [{
                text: 'نمایش همه ی علامت ها'
                //    , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
              }, {
                text: 'نمایش علامت های کتاب  جاری'
                //      ,icon:'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
              }, {
                text: 'نمایش علامت های صفحه جاری'
              }]
            }
          }, '-', {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/next.png',

            tooltip: 'بعدی',

          }, {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/pre.png',

            tooltip: 'قبلی',

          }, '-', {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/delete.png',

            tooltip: 'حذف',
            id: 'deleteBookmarkedNode'

          }, {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/rename.png',

            tooltip: 'تغییر نام',

          }]

          ,
          items: [{
            xtype: 'bookmarkedGrid'
          }]
        }, {
          title: 'حاشیه نویسی',
          layout: 'fit',
          tbar: [{
            xtype: 'toolbar',
            width: '100%',
            items: [{
              xtype: 'splitbutton',
              icon: 'Scripts/resources/ext-theme-classic/images/dd/sort_ascend.png',
              tooltip: 'مرتب کردن',
              handler: function (item) {
                if (item.icon === 'Scripts/resources/ext-theme-classic/images/dd/sort_descend.png') {
                  item.setIcon('Scripts/resources/ext-theme-classic/images/dd/sort_ascend.png');
                } else {
                  item.setIcon('Scripts/resources/ext-theme-classic/images/dd/sort_descend.png');
                }
              },
              menu: {
                xtype: 'menu',
                items: [{
                  text: 'هیچکدام'
                  //    , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
                }, {
                  text: 'براساس عنوان کتاب'
                  //      ,icon:'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
                }, {
                  text: 'براساس شماره صفحه'
                }, {
                  text: 'براساس علامت'
                }]
              }
            }, {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/filter.png',
              cls: 'filter',
              tooltip: 'فیلتر کردن',
              menu: {
                xtype: 'menu',
                id: 'filter_commentedGrid',
                items: [{
                  text: 'نمایش همه ی علامت ها',
                  mode: '0'
                  //    , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
                }, {
                  text: 'نمایش علامت های کتاب  جاری',
                  mode: '1'
                  //      ,icon:'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
                }, {
                  text: 'نمایش علامت های صفحه جاری',
                  mode: '2'
                }]
              }
            }, '-', {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/next.png',

              tooltip: 'بعدی',

            }, {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/pre.png',

              tooltip: 'قبلی',

            }, '-', {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/delete.png',

              tooltip: 'حذف',
              id: 'deleteCommentedNode'

            }]

          }],
          items: [{
            xtype: 'commentedGrid'
          }]
        }],
        listeners: {
          'tabchange': function (tabPanel, tab) {
            tabPanel.setTitle(tab.title);
          }
        }
      }]

    }, {
      region: 'center',
      xtype: 'tabpanel',
      id: 'centerTabpanel',
      rtl: true,
      items: [{
        title: 'متن کتاب',
        collapsible: true,
        layout: {
          type: 'vbox',
          align: 'stretch'
        },
        width: 250,
        height: 200,
        minWidth: 100,
        flex: 1,
        tbar: [{
            xtype: 'label',
            text: 'ابتدای متن'
          }, {
            xtype: 'textfield'
          }, {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/start_line.png',
            cls: 'filter',
            tooltip: 'انتخاب نقطه شروع استخراج متن'
          }, {

            icon: 'Scripts/resources/ext-theme-classic/images/dd/delete.png', // icons can also be specified inline
            cls: 'delete-title',
            tooltip: 'حذف نقطه ابتدای متن'
          }, '-', {
            xtype: 'label',
            text: 'انتهای متن'
          }, {
            xtype: 'textfield'
          }, {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/end_line.png',
            cls: 'filter',
            tooltip: 'انتخاب نقطه پایان استخراج متن'
          }, {

            icon: 'Scripts/resources/ext-theme-classic/images/dd/delete.png', // icons can also be specified inline
            cls: 'delete-title',
            tooltip: 'حذف نقطه انتهای متن'
          }, '-', {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/edit.png', // icons can also be specified inline
            cls: 'delete-title',
            tooltip: 'استخراج متن و مدیا انتخاب شده'

          }, {
            xtype: 'tbfill'
          }, {
            icon: 'Scripts/resources/ext-theme-classic/images/dd/add.png', // icons can also be specified inline
            cls: 'add-title',
            tooltip: 'اضافه کردن صفحه'

          },
          Ext.create('Ext.form.field.ComboBox', {
            displayField: 'label',
            id: 'stampCombo',
            valueField: 'weight',
            width: 100,
            editable: false,
            store: new Ext.data.ArrayStore({
              fields: ['weight', 'label'],
              data: [
                ['25', 'مهم'],
                ['50', 'خیلی مهم'],
                ['75', 'حیاتی']
              ]

            }),
            listConfig: {
              listeners: {
                itemclick: function (list, record) {
                  var btn = Ext.ComponentQuery.query('#btnViewPdf')[0],
                    tree = btn.up('tabpanel').getActiveTab().down('treepanel'),
                    selectionModel = tree.getSelectionModel(),
                    obj = selectionModel.selected.items[0].raw;

                  _.ws.stampBook(obj.id, record.raw[0]);

                }
              }
            },
            queryMode: 'local',
            typeAhead: true
          })
        ],
        items: [{
          xtype: 'panel',
          id: 'bookDetailPanel',
          //html: '',
          minheight: 200,
          flex: 1,
          listeners: {
            afterrender: function () {
              _.isReady('bookDetailPanel');
            }
          },
          dockedItems: [{
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: false
          }]
        }, {
          xtype: 'splitter',
          collapsible: true,
          collapseTarget: 'next'
        }, {
          border: false,
          layout: {
            type: 'hbox',
            align: 'stretch'
          },
          xtype: 'panel',
          title: 'مدیا ها و فایل ها',

          items: [{
              xtype: 'panel',
              width: 350,
              height: 200,
              border: false,
              style: 'padding-top:30px;',
              items: [{
                width: 350,
                border: false,
                xtype: 'documentMediaView'
              }]
            },
            //_.getAudioItem()
            _.getVideoItem('file/movie.mp4')
          ]
        }]
      }, {
        title: 'یادداشت',
        layout: 'fit',
        flex: 1,
        items: [{
          xtype: 'panel',
          layout: 'fit',
          items: [{
            xtype: 'tinymce_textarea',
            height: '700',
            fieldStyle: 'font-family: Courier New; font-size: 12px;',
            noWysiwyg: false,
            tinyMCEConfig: Ext.tinyCfg

          }]
        }]
      }, {
        title: 'جستجو',
        id: 'searchCenterTab',
        layout: {
          type: 'vbox',
          align: 'stretch'
        },

        items: [{
          xtype: 'form',
          frame: true,
          items: [{
            xtype: 'fieldset',
            title: 'عبارت جستجو',
            layout: {
              type: 'vbox',
              align: 'stretch'
            },
            defaults: {
              //padding: 10,
              margin: 5,
              //      border: true,

            },
            items: [{
              layout: 'hbox',
              frame: true,
              items: [{
                xtype: 'textfield',
                id: 'searchCriteria',
                width: '90%'
              }, {
                xtype: 'button',
                text: 'جستجو',
                id: 'btnSearch',
                width: '10%'
              }]
            }, {
              layout: 'hbox',
              frame: true,
              items: [{
                layout: 'hbox',

                items: [{
                  xtype: 'button',
                  text: '"'
                }, {
                  xtype: 'button',
                  text: 'و'
                }, {
                  xtype: 'button',
                  text: 'یا'
                }, {
                  xtype: 'button',
                  text: '-'
                }, {
                  xtype: 'button',
                  text: '%'
                }]
              }, {
                xtype: 'tbfill'

              }, {
                layout: 'hbox',
                frame: true,
                items: [{
                  id: 'autoCorrect-check',
                  xtype: 'checkbox',
                  boxLabel: 'تصحیح خودکار عبارت'
                }, {
                  xtype: 'button',
                  icon: 'Scripts/resources/ext-theme-classic/images/dd/add.png', // icons can also be specified inline
                  cls: 'add-search',
                  tooltip: 'اضافه کردن به لیست جستجو های منتخب'
                }]
              }]
            }]
          }]

        }, {

          xtype: 'tabpanel',
          flex: 2,
          items: [{
            title: 'کتاب',
            frame: true,
            type: 'book',
            layout: {
              type: 'vbox',
              align: 'stretch'
            },
            items: [{

              xtype: 'form',
              frame: true,
              flex: 0.5,
              items: [{
                xtype: 'fieldset',
                title: 'تنظیمات جستجو',
                layout: 'hbox',
                items: [{
                  xtype: 'tbfill'
                }, {
                  id: 'mediaSearch-check',
                  xtype: 'checkbox',
                  style: 'margin-left: 20px',
                  boxLabel: 'فقط اسناد شامل صوت یا تصویر'
                  // , width: '100%'
                }, {
                  id: 'findSynonyms-check',
                  xtype: 'checkbox',
                  //style: 'margin-left: 80px',
                  boxLabel: 'جستجوی مترادف ها'
                  //       , width: '100%'
                }, {
                  xtype: 'tbfill'
                }]
              }]


            }, {
              xtype: 'splitter',
              collapsible: true,
              collapseTarget: 'next',


            }, {
              xtype: 'panel',
              frame: true,
              layout: 'hbox',
              flex: 1,
              title: 'جستجوی پیشرفته',
              items: [{
                xtype: 'tbfill'
              }, {
                xtype: 'fieldset',
                frame: true,
                layout: 'hbox',
                defaults: {
                  padding: 5,
                  margin: 5,
                  border: true,
                  // width: '100%',
                },
                items: [

                  {

                    xtype: 'panel',
                    frame: true,

                    items: [{
                      xtype: 'textfield',
                      emptyText: 'موضوع'
                    }, {
                      xtype: 'textfield',
                      emptyText: 'مترجم'
                    }]
                  }, {

                    xtype: 'panel',
                    frame: true,

                    items: [{
                      xtype: 'textfield',
                      emptyText: 'عنوان کتاب'
                    }, {
                      xtype: 'textfield',
                      emptyText: 'انتشارات'
                    }]
                  }, {

                    xtype: 'panel',
                    frame: true,

                    items: [{
                      xtype: 'textfield',
                      emptyText: 'مولف'
                    }, {
                      xtype: 'textfield',
                      emptyText: 'شماره صفحه'

                    }]
                  }


                ]
              }, {
                xtype: 'tbfill'
              }]
            }, {
              title: 'نتایج جستجو',
              hidden: true,
              id: 'bookSearchTitleId'
            }, {
              flex: 2,

              xtype: 'bookSearchDataView',
              id: 'bookSearchDataViewId',
              title: 'نتایج جستجو',
              hidden: true
            }]

          }, {
            title: 'نمایه',
            xtype: 'form',
            type: 'index',
            border: true,
            frame: true,
            flex: 1,
            layout: {
              type: 'vbox',
              align: 'stretch'
            },
            items: [{
              xtype: 'fieldset',
              title: 'تنظیمات جستجو',
              layout: 'hbox',
              flex: 0.5,
              items: [{
                xtype: 'tbfill'
              }, {
                xtype: 'checkbox',
                style: 'margin-left: 20px',
                boxLabel: 'جستجو در مسیر',
                id: 'searchInPathIndex'

              }, {

                xtype: 'radiogroup',
                id: 'isLeafValueableRadiogroup',
                items: [{
                  name: 'parent',
                  boxLabel: 'اول گره پدر'

                }, {
                  name: 'child',
                  boxLabel: 'اول گره فرزند',
                }],
                width: '30%'


              }, {
                xtype: 'checkbox',

                boxLabel: 'جستجو در متن',
                id: 'searchInTextIndex'


              }, {
                xtype: 'tbfill'
              }]
            }, {
              flex: 2.5,
              xtype: 'indexSearchGrid',
              id: 'indexSearchGrid',
              hidden: true,
              title: 'نتایج جستجو',
              bbar: [{
                xtype: 'toolbar',
                items: [{
                  xtype: 'button'
                }]

              }]

            }]

          }, {
            title: 'تصویر',
            xtype: 'form',
            type: 'media',
            border: true,
            hidden: false,
            frame: true,
            flex: 1,
            layout: {
              type: 'vbox',
              align: 'stretch'
            },
            items: [{
              frame: true,
              defaults: {
                padding: 5,
                margin: 10,
              },
              items: [{
                xtype: 'panel',
                frame: true,
                layout: 'hbox',
                items: [{
                  xtype: 'tbfill'
                }, {
                  xtype: 'textfield',
                  emptyText: 'کد',
                  id: 'codeTextbox'

                }, {
                  xtype: 'textfield',
                  emptyText: 'عنصر',
                  id: 'elementIDTextbox'
                }, {
                  xtype: 'textfield',
                  emptyText: 'موضوع',
                  id: 'subjectTextbox'
                }, {
                  xtype: 'tbfill'
                }]
              }, {
                xtype: 'panel',
                layout: 'hbox',
                frame: true,
                border: false,
                items: [

                  {
                    xtype: 'tbfill'
                  }, {
                    xtype: 'textfield',
                    emptyText: 'کلید واژه',
                    id: 'keyWordsTextbox'

                  }, {
                    xtype: 'textfield',
                    emptyText: 'حضار',
                    id: 'audiencesTextbox'

                  }, {
                    xtype: 'tbfill'
                  }

                ]

              }]
            }, {
              id: 'pictureSearchView',
              xtype: 'pictureSearchView',
              title: 'نتایج جستجو',
              bbar: [{
                xtype: 'toolbar',
                items: [{
                  xtype: 'button'
                }]

              }]

            }]
          }]
        }]
      }]
    }, {
      region: 'west',

      width: 250,
      minWidth: 100,
      height: 200,
      split: true,
      stateful: true,
      stateId: 'mainnav.west',
      collapsible: true,


      layout: 'fit',
      items: [{
        xtype: 'tabpanel',
        tabPosition: 'bottom',

        items: [{
          title: 'کتابخانه',
          // hieght: 200,

          items: [{
              title: 'کتابخانه' //dl.getApplication().getApplication().bundle.getMsg('hi'),
            }, {
              xtype: 'toolbar',
              items: [{

                  icon: 'Scripts/resources/ext-theme-classic/images/dd/drop-add.gif', // icons can also be specified inline
                  cls: 'add-book-library',
                  tooltip: 'اضافه کردن به کتابخانه کاربر',
                  id: 'btnAddToUserLibrary'
                }, {

                  icon: 'Scripts/resources/ext-theme-classic/images/dd/application_view_detail.gif', // icons can also be specified inline
                  cls: 'show-book-detail',
                  tooltip: 'مشخصات کتاب',
                  id: 'btnbookProperties'
                }, {
                  icon: 'Scripts/resources/ext-theme-classic/images/dd/adobe_acrobat.gif',
                  cls: 'open-acrobat',
                  tooltip: 'PDF باز کردن فایل',
                  id: 'btnViewPdf'

                }, '-',

                {

                  icon: 'Scripts/resources/ext-theme-classic/images/dd/document_export.png',
                  tooltip: 'تهییه خروجی',
                  menu: {
                    xtype: 'menu',
                    id: 'exportDocumentLibrary',
                    items: [{
                      text: 'خروجی اکسل',
                      type: 'BookExcel'
                      //    , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
                    }, {
                      text: 'خروجی اکسس',
                      type: 'BookAccess'
                      //      ,icon:'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
                    }, {
                      text: 'خروجی ISO 2709',
                      type: 'BookMARCXML'
                    }, {
                      text: 'خروجی مارک',
                      type: 'BookMARC'
                    }]
                  }
                }, {

                  icon: 'Scripts/resources/ext-theme-classic/images/dd/document_import.png',
                  tooltip: 'تهییه ورودی',
                  menu: {
                    xtype: 'menu',
                    items: [{
                      text: 'ورودی اکسل'
                      //   , icon: 'Scripts/resources/ext-theme-classic/images/dd/excel.gif'
                    }, {
                      text: 'ورودی اکسس'
                      //   , icon: 'Scripts/resources/ext-theme-classic/images/dd/file_export.gif'
                    }, {
                      text: 'ورودی ISO 2709'
                    }, {
                      text: 'ورودی مارک'
                    }]
                  }
                }

              ]
            },

            {

              //we have problem on scroll in height!!!!
              xtype: 'panel',

              items: [{
                xtype: 'bookTree',


              }]

            }
          ]


        }, {
          title: 'کتابخانه کاربر',
          dockedItems: [{
            title: 'کتابخانه کاربر'
          }, {
            xtype: 'toolbar',
            items: [{

              icon: 'Scripts/resources/ext-theme-classic/images/dd/add.png', // icons can also be specified inline
              cls: 'add-title',
              id: 'addCategory',
              tooltip: 'اضافه کردن موضوع جدید'
            }, {

              icon: 'Scripts/resources/ext-theme-classic/images/dd/delete.png', // icons can also be specified inline
              cls: 'delete-title',
              id: 'deleteCategory',
              tooltip: 'حذف'
            }, {
              icon: 'Scripts/resources/ext-theme-classic/images/dd/rename.png',
              cls: 'rename-title',
              id: 'renameCategory',
              tooltip: 'تغییر نام'

            }]
          }],

          items: [{

            xtype: 'userBookTree',

          }]
        }, {
          title: 'نمایه سازی',
          split: true,
          collapsible: true,
          height: 200,
          minWidth: 100,
          layout: {
            type: 'vbox',
            align: 'stretch'
          },
          items: _.getIndexTabPanel()
        }, {

          layout: 'fit',
          height: '100%',
          title: 'فیلم و صوت',
          items: [{
            title: 'فیلم و صوت',
            tbar: {
              xtype: 'trigger',
              triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',

              name: 'search_textfield',
              allowBlank: true,
              onTriggerClick: function () {
                var urls = '/WebServices/MediaService.svc/FilterMedia/' + _.uid() + '/' + this.getValue();
                var store = Ext.getStore('AvagardMediaViewStore');
                store.load({
                  url: urls
                });
              }
            },
            items: [{
              xtype: 'mediaView',
              layout: 'fit'
            }]
          }]

        }]
      }]
    }

  ]
});