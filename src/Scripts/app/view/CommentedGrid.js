﻿Ext.define('dl.view.CommentedGrid', {
  extend: 'Ext.grid.Panel',
  xtype: 'commentedGrid',
  store: 'CommentedStore',
  columns: [{
    text: "عنوان کتاب",
    flex: 1,
    dataIndex: 'BookTile'
  }, {
    text: "صفحه",
    flex: .5,
    dataIndex: 'ID'
  }, {
    text: "عنوان",
    flex: 1,
    dataIndex: 'DocumentTitle'
  }],
  columnLines: true,
  listeners: {
    itemclick: function (grid, record, item, index, event) {
      window._currentCommentedRecord = record;

      var id = record.data['ID'];
      var bookId = record.raw['BookID'];

      _.loadCommentedBook(bookId, id, record);


    }
  }
});