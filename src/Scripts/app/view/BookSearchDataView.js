﻿
Ext.define('dl.view.BookSearchDataView', {
  extend: 'Ext.DataView',

  xtype: 'bookSearchDataView',
  width: 540,
  //    height: 580,
  layout: 'fit',
  autoScroll: true,
  tpl: [
    '<tpl for="." >',
    '<tpl for="FoundItems">',
    '<div id="{BookID:stripTags}" pagenumber="{PageNumberInBook:stripTags}" >',
    '<div style="height:25px;color:#336699;background:#ffffff"><span>{BookTitle:htmlEncode}</span></div>',
    '<div style="height:25px;color:#2245353;"><span>{BookAuthor:htmlEncode}</span></div>',
    '<div style="width: 90%;float:right;">{BookPreviewText}</div>',
    '<div class=" float:left; thumb-bsdv"><img src="/WebServices/{BookCoverImagePath}"></div>',
    '<div onclick="_.openResult(this);"><a style="float:right padding: 6px;" href="#">رفتن به کتاب</a></div>',
    '<div ><a style="float:right padding: 6px;" href="#">درباره کتاب</a></div>',
    '<div ><a style="float:right" href="#">اضافه شدن به کتابخاته شما</a></div></br></br>',
    '</div>',
    '</tpl>',
    '</tpl>'
  ],
  store: 'BookSearchDataViewStore'

});
/*
"BookAuthor"
"BookCoverImagePath"
"BookHasPDF"
"BookID"
"BookPreviewText"
"BookPublishYear"
"BookPublisher"
"BookSubject"
"BookTitle"
"PageNumberInBook"

      "BookAuthor": "",
            "BookCoverImagePath": "DigitalLibraryImageHandler.ashx?bookId=138&getMainImage=true",
            "BookHasPDF": false,
            "BookID": 138,
            "BookPreviewText": "لسنة الأولياء، ومخالف للعقل الانساني،  ومخالف لحقوق الانسان، ومخالف للقوانين السائدة في العالم. وتلك الحكومات لم تقم بعمل حقيقي مطلقا. وينبغي - ان شاء الله - ان تأتي حكومة تعتبر  نفسها مسؤولة <b>امام</b> الشعب، ينبغي ان نؤسس مجلسا ينبثق من الشعب. لم يكن لدينا خلال  الخمسين عاما الماضية مجلسا يمثلنا نحن، فالمجالس النيابية كانت دوما معينة أما من قبل  الاجانب - الذين كانوا يعينون النواب وهو ما ذكره محمد رضا",
            "BookPublishYear": "",
            "BookPublisher": null,
            "BookSubject": "صحیفه امام - عربی",
            "BookTitle": "صحیفه عربی- جلد ششم",
            "PageNumberInBook": "68"
        }
*/
