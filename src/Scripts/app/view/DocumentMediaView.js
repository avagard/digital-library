﻿Ext.define('dl.view.DocumentMediaView', {
  extend: 'Ext.DataView',
  xtype: 'documentMediaView',
  title: 'فایل های سند جاری',
  layout: 'fit',
  autoScroll: true,
  layout: 'fit',
  height: 500,
  itemTpl: [
    '<tpl for="." >',
    '<div class="media-dom document-media" mediaId="{MediaID:stripTags}" bookId="{bookId}" pageNumber="{pageNumber}">',
    '<div style="height:15px;color:#336699;background:#ffffff"><span>{Title:htmlEncode}</span></div>',
    '<tpl for="Files">',
    '<div style="cursor:pointer;" onclick=\'_.sync(this);\' href="{URL:htmlEncode}">{FileName:htmlEncode}</div></br>',
    '</tpl>',

    '</div></br>',
    '</tpl>'
  ],
  store: 'DocumentMediaViewStore'
});