﻿Ext.define('dl.view.PictureSearchView', {
    extend: 'Ext.DataView',

    xtype: 'pictureSearchView',
    title: 'Multisort DataView',
    width: 540,
    height: 580,
    layout: 'fit',

    tpl: [
                '<tpl for=".">',
                    '<div class="thumb-wrap" id="{name:stripTags}">',
                        '<div class="thumb"><img src="/Images/samples/{url}.jpg" title="{name:htmlEncode}"></div>',
                        '<span>{name:htmlEncode}</span>',
                    '</div>',
                '</tpl>',
                '<div class="x-clear"></div>'
    ],
    store: Ext.create('Ext.data.Store', {
        autoLoad: false,
        sortOnLoad: true,
        fields: ['name', 'thumb', 'url', 'type'],
        proxy: {
            type: 'ajax',
            url: '/Scripts/resources/data/pictures-sample-data.json',
            reader: {
                type: 'json',
                root: ''
            }
        }
    })
});