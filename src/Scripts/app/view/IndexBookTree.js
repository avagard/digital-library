(function () {
  var indexBookTreeContextMenu = new Ext.menu.Menu({
    items: [{
      text: 'حذف',
      handler: function () {
        var menu = this.up('menu'),
          data = menu.currentNode.raw,
          tree = menu.treeView,
          secondParams,
          methodName;
        if (!data.IsIndex) {
          methodName = 'DeleteNode';
        } else {

          methodName = 'DeleteIndex';
        }
        secondParams = data.id;
        var urls = '/WebServices/IndexService.svc/' + methodName + '/' + _.uid() + '/' + secondParams + '/' +
          tree.up('treepanel').tabId;

        Ext.Ajax.request({
          url: urls,
          myTree: tree,
          success: function (response) {

            response.request.options.myTree.getStore().treeStore.reload();
          },
          failure: function (result, request) {
            _.alert(result.responseText);
          }

        });
      }

    }, {
      text: 'بریدن'
    }, {
      text: 'افزودن متن انتخابی به نمایه',
      handler: function () {

        var menu = this.up('menu'),
          data = menu.currentNode.raw;
        _.addIndex(data);
      }
    }, {
      text: 'نمایش نمایه',
      handler: function () {
        _.highlightSavedIndex();
      }
    }]
  });

  var IndexBookTree = Ext.define('dl.view.IndexBookTree', {
    extend: 'Ext.tree.Panel',
    xtype: 'indexBookTree'
  });

  _.getIndexBookTree = function (tabId, options) {
    var store = Ext.data.TreeStore.create({
      autoLoad: false,
      proxy: {
        type: 'rest',
        url: '/WebServices/DigitalLibraryRESTService.svc/GetIndexBooksTree/' + _.uid() + '/' + tabId,
        reader: {
          root: 'List'
        }
      }
    });

    return IndexBookTree.create({
      flex: 1,
      id: 'indextree_' + tabId,
      action: 'indexBookTree',
      store: store,
      tbar: {
        xtype: 'trigger',
        triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',

        name: 'search_textfield',
        allowBlank: true,
        onTriggerClick: function () {

          _.ws.filterIndexPathInTree(this.getValue(), tabId, function (tabId, params) {

            console.log(params);
            // var store = Ext.getStore('indexStore_' + tabId);
            store.loadData(params);
          });

        }
      },
      rootVisible: false,
      rtl: true,
      autoScroll: true,
      containerScroll: true,
      useArrows: true,
      autoheight: true,
      height: 700,
      viewConfig: {
        plugins: {
          ptype: 'treeviewdragdrop',
        },

        listeners: {
          beforedrop: function (node, data, overModel, dropPosition, dropFunction) {
            if (dropPosition === 'append') {

              var secondParams,
                methodName;
              if (!data.records[0].raw.IsIndex) {
                secondParams = data.records[0].raw.text;
                methodName = 'MoveNode';
              } else {
                secondParams = data.records[0].raw.id;
                methodName = 'MoveIndex';
              }

              var urls = '/WebServices/IndexService.svc/' + methodName + '/' + _.uid() + '/' + secondParams + '/' +
                overModel.raw.id + '/' + this.up('treepanel').tabId;

              Ext.Ajax.request({
                url: urls,
                success: function (response) {

                  if (response.responseText !== true) {

                  }
                },
                failure: function (result, request) {
                  _.alert(result.responseText);
                }

              });
              return true;

            } else {
              return false;
            }

          },
          drop: function (node, data, model, dropPosition, opts) {

          }
        }
      },
      listeners: {
        checkchange: function (node, checked) {
          node.eachChild(function (n) {
            n.set({
              "checked": checked
            });
            n.eachChild(function (m) {
              m.set({
                "checked": checked
              });
            });
          });
        },
        itemcontextmenu: function (view, record, item, index, event, eOpts) {
          indexBookTreeContextMenu.showAt(event.getXY());
          indexBookTreeContextMenu.currentNode = record;
          indexBookTreeContextMenu.treeView = view;
          event.stopEvent();
        }
      }
    });
  };
}());