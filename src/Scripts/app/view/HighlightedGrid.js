﻿Ext.define('dl.view.HighlightedGrid', {
  extend: 'Ext.grid.Panel',
  xtype: 'highlightedGrid',
  id: 'highlightedGrid',
  store: 'HighlightStore',
  columns: [{
      text: "عنوان کتاب",
      flex: 1,
      dataIndex: 'BookTile'
    }, {
      text: "صفحه",
      flex: .5,
      dataIndex: 'DocumentTitle'
    }

  ],
  columnLines: true,
  loadMask: true,
  stripeRows: true,

  listeners: {
    itemclick: function (grid, record, item, index, event) {
      var id = record.data['ID'],
        bookId = record.raw['BookID'];

      _.loadBookColorized(bookId, id, record);
    }
  }
});