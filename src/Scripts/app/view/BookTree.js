Ext.define('dl.view.BookTree', {
  extend: 'Ext.tree.Panel',
  xtype: 'bookTree',
  store: 'BookTreeStore',
  rootVisible: false,
  // lines: true,
  rtl: true,
  autoScroll: true,
  containerScroll: true,
  useArrows: true,
  autoheight: true,
  height: 700,
  listeners: {
    itemclick: function (view, rec, item, index, eventObj) {
      var bookPagesUrl,
        selectionModel = view.getSelectionModel(),
        obj = selectionModel.selected.items[0].raw,
        nodeId = obj.id,
        objArr;

      if (obj.NodeType === 1) {
        _.loadBook(nodeId, 1);
        _.ws.hasPDF(nodeId, function (data) {
          var btn = Ext.ComponentQuery.query('#btnViewPdf')[0];
          if (!data) {
            btn.setDisabled(true);
          } else {
            btn.setDisabled(false);
          }

        });
        _.ws.getBookStamp(obj['id'], function (data) {
          var combo = Ext.ComponentQuery.query('#stampCombo')[0];
          var res = data + "";
          if (res === 0 || res === -1) {
            res = "";
          }
          combo.setValue(res);
        });
      }

      if (rec.get('leaf')) {
        objArr = rec.get('id').split('_');
        _.loadBook(Number(objArr[0]), Number(objArr[1]));
      }
    }
  }
});