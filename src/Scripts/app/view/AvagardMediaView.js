﻿Ext.define('dl.view.AvagardMediaView', {
  extend: 'Ext.DataView',
  xtype: 'mediaView',
  title: 'فیلم و صوت',
  autoScroll: true,
  height: '100%',
  itemTpl: [
    '<tpl for="." >',
    '<div class="media-dom avagard-media" mediaId="{MediaID:stripTags}" bookId="{bookId:stripTags}" pageNumber="{pageNumber:stripTags}">',
    '<div style="height:15px;color:#336699;background:#ffffff"><span>{Title:htmlEncode}</span></div>',
    '<tpl for="Files">',
    '<div style="cursor:pointer;" onclick=\'_.sync(this);\' href="{URL:htmlEncode}">',
    '{FileName:htmlEncode}',
    '</div>',
    '</br>',
    '</tpl>',

    '</div></br>',
    '</tpl>'
  ],
  store: 'AvagardMediaViewStore'
});