Ext.define('dl.view.UserBookTree', {
  extend: 'Ext.tree.Panel',
  xtype: 'userBookTree',
  store: 'UserBookTreeStore',
  rootVisible: false,
  // lines: true,
  rtl: true,
  autoScroll: true,
  containerScroll: true,
  useArrows: true,
  autoheight: true,
  selType: 'cellmodel',
  height: 700,
  viewConfig: {
    plugins: {
      ptype: 'treeviewdragdrop',
      appendOnly: true
    },
    listeners: {
      beforedrop: function (node, data, overModel, dropPosition, dropFunction) {
        var row = data.records[0],
          id = row.get('id'),
          overModelId = overModel.get('id'),
          parentId = row.get('parentId'),
          iconCls = row.get('iconCls');

        //id = id.slice(4);
        overModelId = overModelId.slice(4);

        if (parentId.indexOf('Cat_') === 0) {
          parentId = parentId.slice(4);
        }

        if (iconCls === 'book-node') {
          if (dropPosition === 'append') {
            var urls = '/WebServices/BookService.svc/MoveBookToCategory/' + _.uid() + '/' + id + '/' + parentId + '/' + overModelId;

            Ext.Ajax.request({
              url: urls,
              success: function (response) {
                if (response.responseText === 'false') {
                  console.error('Mr. Mirhossini WTF??? It works but you are returning false!!');
                  console.warn('Mr. Mirhossini WTF??? It works but you are returning false!!');
                  console.error('Mr. Mirhossini WTF??? It works but you are returning false!!');

                }
                //_.alert(_.getMsg('serverDropException'));
              },
              failure: function (result, request) {
                _.alert(result.responseText);
              }

            });
            return true;
          }
        }

        //   _.alert($getMsg('dragAndDropFailed'));
        return false;
      },
      drop: function (node, data, model, dropPosition, opts) {


      }
    }
  },
  listeners: {
    itemclick: function (view, rec, item, index, eventObj) {

      var selectionModel = view.getSelectionModel(),
        selected = selectionModel.selected.items[0],
        id = selected.get('id');

      if (rec.get('leaf') == true) {
        var objArr = rec.get('id').split('_');
        _.loadBook(Number(objArr[0]), Number(objArr[1]));
      }

      id = id.slice(4);

      var url2 = '/WebServices/BookService.svc/GetBookStamp/' + _.uid() + '/' + id;
      Ext.Ajax.request({
        url: url2,
        success: function (response) {
          var combo = Ext.ComponentQuery.query('#stampCombo')[0];
          var res = response.responseText;
          if (res == 0 || res == -1) res = "";
          combo.setValue(res);
        },
        failure: function (result, request) {
          _.alert(result.responseText);
        }

      });

    }

  }
});