﻿Ext.define('dl.view.IndexSearchGrid', {

  extend: 'Ext.DataView',
  xtype: 'indexSearchGrid',
  store: 'IndexSearchDataViewStore',
  width: 540,
  //    height: 580,
  layout: 'fit',
  autoScroll: true,
  tpl: ['<tpl for="." >',
    '<tpl for="FoundItems">',
    '<div id="{IndexID:stripTags}" IndexTextRTF="{IndexTextRTF:stripTags}" >',
    '<div id="{Length:stripTags}" indexid="{IndexID:stripTags}" OwnerDocumentID="{OwnerDocumentID:stripTags}" StartPosition="{StartPosition:stripTags}" >',
    '<div style="height:25px;color:#336699;background:#ffffff"><span>{IndexTitle:htmlEncode}</span></div>',

    '<div style="width: 90%;float:right;">{Path}</div>',
    '<div onclick="_.openIndexResult(this);"><a style="float:right padding: 6px;" href="#">رفتن به کتاب</a></div>',
    '</br>',
    '</br>',
    '</div>',
    '</tpl>',
    '</tpl>'
  ]



});
/*
{
    "FoundItems": [
        {
            "": 4095,
            "Path": "تبیان -> روحانیت و حوزه‏هاى علمیه از دیدگاه امام خمینى(س) -> هویت روحانیت -> روحانیت، دژ اسلام و سنگر مقاومت",
            "": 195
        },
        {
            "IndexID": 43355,
            "IndexTextRTF": "{\\rtf1\\fbidis\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset178 Tahoma;}{\\f1\\fnil\\fcharset0 Tahoma;}}\r\n{\\colortbl ;\\red105\\green105\\blue105;\\red0\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\ltrpar\\cf1\\f0\\rtlch\\fs17\\'c2\\'d2\\'c7\\'cf\\'ed \\'e3\\'ed\\'fe\\'ce\\'e6\\'c7\\'e5\\'ed\\'e3 \\'e3\\'c7. \\'e3\\'c7 \\'81\\'e4\\'cc\\'c7\\'e5 \\'d3\\'c7\\'e1 \\'c7\\'d3\\'ca \\'98\\'e5 \\'cf\\'d1 \\'c7\\'ce\\'ca\\'e4\\'c7\\'de \\'c8\\'e5 \\'d3\\'d1 \\'c8\\'d1\\'cf\\'ed\\'e3. \\'e4\\'e5 \\'e3\\'d8\\'c8\\'e6\\'da\\'c7\\'ca \\'cf\\'c7\\'d4\\'ca\\'ed\\'e3\\'a1 \\'e4\\'e5 \\'d1\\'c7\\'cf\\'ed\\'e6 \\'d5\\'cd\\'ed\\'cd \\'cf\\'c7\\'d4\\'ca\\'ed\\'e3\\'a1 \\'e4\\'e5 \\'ca\\'e1\\'e6\\'ed\\'d2\\'ed\\'e6\\'e4 \\'d5\\'cd\\'ed\\'cd \\'cf\\'c7\\'d4\\'ca\\'ed\\'e3\\'ba \\'e4\\'e5 \\'ce\\'d8\\'ed\\'c8 \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca \\'cd\\'d1\\'dd \\'c8\\'d2\\'e4\\'cf\\'a1 \\'e4\\'e5 \\'c7\\'e5\\'e1 \\'e3\\'e4\\'c8\\'d1 \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca\\'e4\\'cf \\'cd\\'d1\\'dd \\'c8\\'d2\\'e4\\'e4\\'cf\\'a1 \\'e4\\'e5 \\cf2\\b\\fs16\\'c7\\'e3\\'c7\\'e3\\cf1\\b0\\fs17  \\'cc\\'e3\\'c7\\'da\\'ca \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca \\'c2\\'d2\\'c7\\'cf \\'98\\'c7\\'d1 \\'ce\\'e6\\'cf\\'d4 \\'d1\\'c7 \\'c7\\'cf\\'c7\\'e3\\'e5 \\'c8\\'cf\\'e5\\'cf\\'ba \\'e4\\'e5 \\'e5\\'ed\\'8d \\'ed\\'98 \\'c7\\'d2 \\'c7\\'de\\'d4\\'c7\\'d1 \\'e3\\'e1\\'ca \\'98\\'c7\\'d1\\'d4\\'c7\\'e4 \\'d1\\'c7 \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca\\'e4\\'cf \\'c7\\'cf\\'c7\\'e3\\'e5 \\'c8\\'cf\\'e5\\'e4\\'cf. \\'e6 \\'cf\\'d1 \\'d2\\'e3\\'c7\\'e4 \\'c7\\'ed\\'d4\\'c7\\'e4 \\'e5\\'e3 \\'e5\\'e3\\'ed\\'e4 \\'c7\\'ce\\'ca\\'e4\\'c7\\'de \\'c8\\'e5 \\'d8\\'d1\\'ed\\'de \\'c8\\'c7\\'e1\\'c7\\'ca\\'d1 \\'c8\\'c7\\'de\\'ed \\'c7\\'d3\\'ca \\'e6 \\'c8\\'c7\\'de\\'ed \\'c8\\'e6\\'cf. \\'e6 \\'c7\\'e1\\'c2\\'e4 \\'e5\\'e3 \\'c8\\'c7\\'d2 \\'e4\\'ed\\'e3\\u1728? \\'cd\\'d4\\'c7\\'d4\\u1728?  \\'c7\\'e6 \\'98\\'e5\\f1\\ltrch\\par\r\n}\r\n",
            "IndexTitle": "حاکمیت اختناق بر رسانه‏ها",
            "Length": 499,
            "OwnerDocumentID": 4071,
            "Path": "تبیان -> تبلیغات (هنر و رسانه‏ها) از دیدگاه امام خمینى(س) -> رسانه‏ها در عصر پهلوى -> خدمت به منافع رژیم",
            "StartPosition": 206
        },
        {
            "IndexID": 47102,
            "IndexTextRTF": "{\\rtf1\\fbidis\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset178 Tahoma;}{\\f1\\fnil\\fcharset0 Tahoma;}}\r\n{\\colortbl ;\\red105\\green105\\blue105;\\red0\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\ltrpar\\cf1\\f0\\rtlch\\fs17\\'c7\\'d2 \\'c2\\'de\\'c7\\'ed\\'c7\\'e4 \\'da\\'e1\\'e3\\'c7 \\'e6 \\'c7\\'dd\\'c7\\'d6\\'e1 \\'c8\\'c7\\'ed\\'cf \\'ca\\'d4\\'98\\'d1 \\'98\\'e4\\'e3. \\'81\\'ed\\'d1\\'e6\\'d2\\'ed \\'e3\\'e1\\'ca \\'e3\\'d1\\'e5\\'e6\\'e4 \\'c7\\'de\\'cf\\'c7\\'e3\\'c7\\'ca \\'da\\'e1\\'e3\\'c7 \\'dc \\'c7\\'e6\\'e1\\'c7\\'f0 \\'dc \\'e6 \\'d3\\'c7\\'ed\\'d1 \\'d8\\'c8\\'de\\'c7\\'ca \\'dc \\'cb\\'c7\\'e4\\'ed\\'c7\\'f0 \\'dc \\'c8\\'e6\\'cf\\'e5 \\'c7\\'d3\\'ca. \\'d4\\'e3\\'c7 \\'da\\'e1\\'e3\\'c7 \\'e5\\'e3\\'c7\\'e4 \\'d8\\'e6\\'d1\\'ed \\'98\\'e5 \\'e6\\'d9\\'ed\\'dd\\u1728? \\'d4\\'d1\\'da\\'ed\\'fe\\'ca\\'c7\\'e4 \\'e5\\'d3\\'ca \\'98\\'e5 \\cf2\\b\\fs16\\'c7\\'e3\\'c7\\'e3\\cf1\\b0\\fs17  \\'c7\\'e3\\'ca \\'c8\\'c7\\'d4\\'ed\\'cf\\'a1 \\'81\\'ed\\'d4\\'de\\'cf\\'e3 \\'c8\\'c7\\'d4\\'ed\\'cf \\'cf\\'d1 \\'e3\\'d3\\'c7\\'c6\\'e1 \\'c7\\'e3\\'ca\\'a1 \\'cf\\'dd\\'da \\'98\\'e4\\'ed\\'cf \\'e3\\'dd\\'c7\\'d3\\'cf \\'d1\\'c7 \\'c7\\'d2 \\'e3\\'e1\\'ca\\'e5\\'c7\\'a1 \\'c8\\'cd\\'e3\\'cf\\'c7\\'e1\\'e1\\'f8\\'e5\\'fe \\'de\\'ed\\'c7\\'e3 \\'c8\\'e5 \\'c7\\'e3\\'d1 \\'dd\\'d1\\'e3\\'e6\\'cf\\'ed\\'cf. \\'e6 \\'e3\\'e4 \\'c7\\'d2 \\'de\\'f6\\'c8\\'f3\\'e1 \\'e3\\'e1\\'ca \\'d4\\'d1\\'ed\\'dd \\'c7\\'ed\\'d1\\'c7\\'e4 \\'c7\\'d2 \\'d4\\'e3\\'c7 \\'ca\\'d4\\'98\\'d1 \\'e3\\'ed\\'fe\\'98\\'e4\\'e3. \\'ce\\'cf\\'c7\\'e6\\'e4\\'cf \\'c7\\'e4\\'fe\\'d4\\'c7\\'c1\\'c7\\'e1\\'e1\\'f8\\'e5\\'fe \\'d1\\'e6\\'cd\\'c7\\'e4\\'ed\\'ca \\'d1\\'c7 \\'98\\'e5 \\'d0\\'ce\\'ed\\'d1\\u1728? \\'e3\\'e1\\'ca \\'c7\\'d3\\'ca\\'a1 \\'81\\'d4\\'ca\\'e6\\'c7\\'e4\\u1728? \\'e3\\'e1\\'ca \\'c7\\'d3\\'ca\\'a1 \\'cd\\'dd\\'d9 \\'98\\'e4\\'cf \\'e6 \\'de\\'e6\\'f8\\'ca \\'c8\\'e5 \\'c7\\'e6 \\'c8\\'cf\\'e5\\'cf\\f1\\ltrch\\par\r\n}\r\n",
            "IndexTitle": "وظیفه شرعى علما در امامت امت",
            "Length": 402,
            "OwnerDocumentID": 4095,
            "Path": "تبیان -> تحولات اجتماعى و انقلاب اسلامى از دیدگاه امام خمینى(س) -> تحول در نظام اجتماعى -> عوامل تأثیرگذار (کارگزاران تغییر) -> نخبگان مذهبى ـ سیاسى",
            "StartPosition": 195
        },
        {
            "IndexID": 48834,
            "IndexTextRTF": "{\\rtf1\\fbidis\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset178 Tahoma;}{\\f1\\fnil\\fcharset0 Tahoma;}}\r\n{\\colortbl ;\\red105\\green105\\blue105;\\red0\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\ltrpar\\cf1\\f0\\rtlch\\fs17\\'c2\\'d2\\'c7\\'cf\\'ed \\'e3\\'ed\\'fe\\'ce\\'e6\\'c7\\'e5\\'ed\\'e3 \\'e3\\'c7. \\'e3\\'c7 \\'81\\'e4\\'cc\\'c7\\'e5 \\'d3\\'c7\\'e1 \\'c7\\'d3\\'ca \\'98\\'e5 \\'cf\\'d1 \\'c7\\'ce\\'ca\\'e4\\'c7\\'de \\'c8\\'e5 \\'d3\\'d1 \\'c8\\'d1\\'cf\\'ed\\'e3. \\'e4\\'e5 \\'e3\\'d8\\'c8\\'e6\\'da\\'c7\\'ca \\'cf\\'c7\\'d4\\'ca\\'ed\\'e3\\'a1 \\'e4\\'e5 \\'d1\\'c7\\'cf\\'ed\\'e6 \\'d5\\'cd\\'ed\\'cd \\'cf\\'c7\\'d4\\'ca\\'ed\\'e3\\'a1 \\'e4\\'e5 \\'ca\\'e1\\'e6\\'ed\\'d2\\'ed\\'e6\\'e4 \\'d5\\'cd\\'ed\\'cd \\'cf\\'c7\\'d4\\'ca\\'ed\\'e3\\'ba \\'e4\\'e5 \\'ce\\'d8\\'ed\\'c8 \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca \\'cd\\'d1\\'dd \\'c8\\'d2\\'e4\\'cf\\'a1 \\'e4\\'e5 \\'c7\\'e5\\'e1 \\'e3\\'e4\\'c8\\'d1 \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca\\'e4\\'cf \\'cd\\'d1\\'dd \\'c8\\'d2\\'e4\\'e4\\'cf\\'a1 \\'e4\\'e5 \\cf2\\b\\fs16\\'c7\\'e3\\'c7\\'e3\\cf1\\b0\\fs17  \\'cc\\'e3\\'c7\\'da\\'ca \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca \\'c2\\'d2\\'c7\\'cf \\'98\\'c7\\'d1 \\'ce\\'e6\\'cf\\'d4 \\'d1\\'c7 \\'c7\\'cf\\'c7\\'e3\\'e5 \\'c8\\'cf\\'e5\\'cf\\'ba \\'e4\\'e5 \\'e5\\'ed\\'8d \\'ed\\'98 \\'c7\\'d2 \\'c7\\'de\\'d4\\'c7\\'d1 \\'e3\\'e1\\'ca \\'98\\'c7\\'d1\\'d4\\'c7\\'e4 \\'d1\\'c7 \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca\\'e4\\'cf \\'c7\\'cf\\'c7\\'e3\\'e5 \\'c8\\'cf\\'e5\\'e4\\'cf. \\'e6 \\'cf\\'d1 \\'d2\\'e3\\'c7\\'e4 \\'c7\\'ed\\'d4\\'c7\\'e4 \\'e5\\'e3 \\'e5\\'e3\\'ed\\'e4 \\'c7\\'ce\\'ca\\'e4\\'c7\\'de \\'c8\\'e5 \\'d8\\'d1\\'ed\\'de \\'c8\\'c7\\'e1\\'c7\\'ca\\'d1 \\'c8\\'c7\\'de\\'ed \\'c7\\'d3\\'ca \\'e6 \\'c8\\'c7\\'de\\'ed \\'c8\\'e6\\'cf. \\'e6 \\'c7\\'e1\\'c2\\'e4 \\'e5\\'e3 \\'c8\\'c7\\'d2 \\'e4\\'ed\\'e3\\u1728? \\'cd\\'d4\\'c7\\'d4\\u1728?  \\'c7\\'e6 \\'98\\'e5\\f1\\ltrch\\par\r\n}\r\n",
            "IndexTitle": "فداکارى براى کسب آزادى",
            "Length": 499,
            "OwnerDocumentID": 4071,
            "Path": "تبیان -> آزادى در اندیشه امام خمینى(س) -> جایگاه آزادى -> آزادیهاى فردى و سیاسى",
            "StartPosition": 206
        },
        {
            "IndexID": 51120,
            "IndexTextRTF": "{\\rtf1\\fbidis\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset178 Tahoma;}{\\f1\\fnil\\fcharset0 Tahoma;}}\r\n{\\colortbl ;\\red105\\green105\\blue105;\\red0\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\ltrpar\\cf1\\f0\\rtlch\\fs17\\'81\\'e4\\'cc\\'c7\\'e5 \\'d3\\'c7\\'e1 \\'c7\\'d3\\'ca \\'98\\'e5 \\'cf\\'d1 \\'c7\\'ce\\'ca\\'e4\\'c7\\'de \\'c8\\'e5 \\'d3\\'d1 \\'c8\\'d1\\'cf\\'ed\\'e3. \\'e4\\'e5 \\'e3\\'d8\\'c8\\'e6\\'da\\'c7\\'ca \\'cf\\'c7\\'d4\\'ca\\'ed\\'e3\\'a1 \\'e4\\'e5 \\'d1\\'c7\\'cf\\'ed\\'e6 \\'d5\\'cd\\'ed\\'cd \\'cf\\'c7\\'d4\\'ca\\'ed\\'e3\\'a1 \\'e4\\'e5 \\'ca\\'e1\\'e6\\'ed\\'d2\\'ed\\'e6\\'e4 \\'d5\\'cd\\'ed\\'cd \\'cf\\'c7\\'d4\\'ca\\'ed\\'e3\\'ba \\'e4\\'e5 \\'ce\\'d8\\'ed\\'c8 \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca \\'cd\\'d1\\'dd \\'c8\\'d2\\'e4\\'cf\\'a1 \\'e4\\'e5 \\'c7\\'e5\\'e1 \\'e3\\'e4\\'c8\\'d1 \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca\\'e4\\'cf \\'cd\\'d1\\'dd \\'c8\\'d2\\'e4\\'e4\\'cf\\'a1 \\'e4\\'e5 \\cf2\\b\\fs16\\'c7\\'e3\\'c7\\'e3\\cf1\\b0\\fs17  \\'cc\\'e3\\'c7\\'da\\'ca \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca \\'c2\\'d2\\'c7\\'cf \\'98\\'c7\\'d1 \\'ce\\'e6\\'cf\\'d4 \\'d1\\'c7 \\'c7\\'cf\\'c7\\'e3\\'e5 \\'c8\\'cf\\'e5\\'cf\\'ba \\'e4\\'e5 \\'e5\\'ed\\'8d \\'ed\\'98 \\'c7\\'d2 \\'c7\\'de\\'d4\\'c7\\'d1 \\'e3\\'e1\\'ca \\'98\\'c7\\'d1\\'d4\\'c7\\'e4 \\'d1\\'c7 \\'e3\\'ed\\'fe\\'ca\\'e6\\'c7\\'e4\\'d3\\'ca\\'e4\\'cf \\'c7\\'cf\\'c7\\'e3\\'e5 \\'c8\\'cf\\'e5\\'e4\\'cf. \\'e6 \\'cf\\'d1 \\'d2\\'e3\\'c7\\'e4 \\'c7\\'ed\\'d4\\'c7\\'e4 \\'e5\\'e3 \\'e5\\'e3\\'ed\\'e4 \\'c7\\'ce\\'ca\\'e4\\'c7\\'de \\'c8\\'e5 \\'d8\\'d1\\'ed\\'de \\'c8\\'c7\\'e1\\'c7\\'ca\\'d1 \\'c8\\'c7\\'de\\'ed \\'c7\\'d3\\'ca \\'e6 \\'c8\\'c7\\'de\\'ed \\'c8\\'e6\\'cf\\f1\\ltrch\\par\r\n}\r\n",
            "IndexTitle": "اختناق ـ 4",
            "Length": 348,
            "OwnerDocumentID": 4071,
            "Path": "تبیان -> تاریخ معاصر ایران از دیدگاه امام خمینى(س) -> دورۀ محمدرضا شاه -> حکومت محمدرضا شاه -> اختناق",
            "StartPosition": 280
        },
        {
            "IndexID": 38704,
            "IndexTextRTF": "{\\rtf1\\fbidis\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset178 Tahoma;}{\\f1\\fnil\\fcharset0 Tahoma;}}\r\n{\\colortbl ;\\red105\\green105\\blue105;\\red0\\green0\\blue0;}\r\n\\viewkind4\\uc1\\pard\\ltrpar\\cf1\\f0\\rtlch\\fs17\\'de\\'ed\\'d5\\'d1 \\'e6 \\'e3\\'cd\\'d1\\'c7\\'c8 \\'e3\\'c7\\'e1 \\'c2\\'ce\\'e6\\'e4\\'cf! \\'dc \\'e3\\'cd\\'d1\\'c7\\'c8 \\'e5\\'e3 \\'e4\\'e3\\'ed\\'fe\\'90\\'d0\\'c7\\'d1\\'e4\\'cf \\'c8\\'d1\\'c7\\'ed \\'e3\\'c7 \\'c8\\'c7\\'d4\\'cf \\'dc \\'c7\\'d3\\'e1\\'c7\\'e3 \\'cf\\'ed\\'e4 \\'d3\\'ed\\'c7\\'d3\\'ca \\'c7\\'d3\\'ca\\'ba \\'cd\\'98\\'e6\\'e3\\'ca \\'cf\\'c7\\'d1\\'cf. \\'d4\\'e3\\'c7 \\'c8\\'ce\\'d4\\'e4\\'c7\\'e3\\u1728? \\'cd\\'d6\\'d1\\'ca \\'c7\\'e3\\'ed\\'d1 \\'e6 \\'98\\'ca\\'c7\\'c8 \\'cd\\'d6\\'d1\\'ca \\'c7\\'e3\\'ed\\'d1 \\'c8\\'e5 \\'e3\\'c7\\'e1\\'98 \\'c7\\'d4\\'ca\\'d1 \\'d1\\'c7 \\'c8\\'ce\\'e6\\'c7\\'e4\\'ed\\'cf \\'c8\\'c8\\'ed\\'e4\\'ed\\'cf \\'8d\\'ed\\'d3\\'ca. \\'cf\\'d3\\'ca\\'e6\\'d1\\'e5\\'c7\\'ed \\'81\\'ed\\'db\\'e3\\'c8\\'d1 \\'e6 \\'cf\\'d3\\'ca\\'e6\\'d1\\'e5\\'c7\\'ed \\cf2\\b\\fs16\\'c7\\'e3\\'c7\\'e3\\cf1\\b0\\fs17  \\'dc \\'da\\'e1\\'ed\\'e5 \\'c7\\'e1\\'d3\\'e1\\'c7\\'e3 \\'dc \\'cf\\'d1 \\'cc\\'e4\\'90\\'e5\\'c7 \\'e6 \\'cf\\'d1 \\'d3\\'ed\\'c7\\'d3\\'c7\\'ca \\'c8\\'c8\\'ed\\'e4\\'ed\\'cf \\'8d\\'e5 \\'cf\\'c7\\'d1\\'cf. \\'c7\\'ed\\'e4 \\'d0\\'ce\\'c7\\'ed\\'d1 \\'d1\\'c7 \\'e3\\'c7 \\'cf\\'c7\\'d1\\'ed\\'e3\\'a1 \\'da\\'f5\\'d1\\'d6\\u1728? \\'c7\\'d3\\'ca\\'dd\\'c7\\'cf\\'e5\\'fe\\'c7\\'d4 \\'d1\\'c7 \\'e4\\'cf\\'c7\\'d1\\'ed\\'e3. \\'d0\\'ce\\'ed\\'d1\\'e5 \\'e3\\'e6\\'cc\\'e6\\'cf \\'c7\\'d3\\'ca\\'a1 \\'e5\\'e3\\'e5 \\'8d\\'ed\\'d2 \\'cf\\'c7\\'d1\\'ed\\'e3\\'a1 \\'da\\'f5\\'d1\\'d6\\u1728? \\'c7\\'d3\\'ca\\'dd\\'c7\\'cf\\'e5 \\'e4\\'cf\\'c7\\'d1\\'ed\\'e3. \\'e3\\'cb\\'e1 \\'c7\\'ed\\'e4\\'98\\'e5 \\'c7\\'ed\\'d1\\'c7\\'e4 \\'e5\\'e3\\'e5 \\'8d\\'ed\\'d2 \\'cf\\'c7\\'d1\\'cf \\'c7\\'e3\\'c7 \\'e3\\'ed\\'fe\\'cf\\'e5\\'e4\\'cf\\f1\\ltrch\\par\r\n}\r\n",
            "IndexTitle": "مباحث اجتماعى و سیاسى در حدیث و فقه",
            "Length": 890,
            "OwnerDocumentID": 4098,
            "Path": "تبیان -> حکومت اسلامى و ولایت فقیه در اندیشۀ امام خمینى(س) -> دین و سیاست -> اسلام، دین سیاست",
            "StartPosition": 70
        }
    ],
    "SearchPhrase": "امام",
    "TotalFoundEntries": 6,
    "TotalPages": 1
}*/