(function (global, $) {
  _.getVideoItem = function () {
    return new Ext.Panel({
      border: false,
      minheight: 225,
      height: 225,
      html: '<video width=\'300px\' height=\'240\' controls>' +
        '<source src=\'\' type=\'video/mp4\'>' +
        '</video>'

    });
  };

  _.play = function play(filePath) {
    var $vid = _.$vid(),
      vid = _.vid(),
      source = $vid.find('source').get(0),
      dfd = $.Deferred();

    if (!_.vid().paused) {
      _.vid().pause();
    }

    source.src = filePath;
    vid.src = filePath;
    vid.play();

    if (vid.readyState === vid.HAVE_ENOUGH_DATA) {
      dfd.resolve(true);
    } else {
      $vid.on('playing', function onplaying() {
        $vid.off('playing', onplaying);
        dfd.resolve(true);
      });
    }
    return dfd.promise();
  };

  _.clear = function clear() {
    var source = _.$vid().find('source').get(0);
    if (!_.vid().paused) {
      _.vid().pause();
    }

    source.src = '';
    _.vid().src = '';
  };

}(window, jQuery));