(function (global, $) {

  function bindEvents() {
    _.$vid()
      .on('playing', function () {
        _.sync.play();
      })
      .on('pause', function () {
        _.sync.pause();
      })
      .on('seeking', function (e) {
        _.sync.seeking(e);
      })
      .on('seeked', function (e) {
        _.sync.seeked(e);
      })
      .on('ended', function (e) {
        _.sync.ended(e);
      });
  }

  function unbindEvents() {
    _.$vid()
      .off('playing')
      .off('pause')
      .off('ended')
      .off('seeking')
      .off('seeked');
  }

  function getPageTags(pageNumber) {
    var data = _.sync.data,
      i = 0,
      len = data.pages.length;

    if (!pageNumber) {
      pageNumber = data.pageNumber;
    }

    if (getPageTags.tags === undefined) {
      getPageTags.tags = {};
    }
    if (getPageTags.tags[pageNumber]) {
      return getPageTags.tags[pageNumber];
    }

    for (; i < len; i += 1) {
      if (data.pages[i].pageNumber === pageNumber) {
        return (getPageTags.tags[pageNumber] = data.pages[i].Tags);
      }
    }
    return null;
  }

  function deromanize(str) {
    str = str.toUpperCase();
    var validator = /^M*(?:D?C{0,3}|C[MD])(?:L?X{0,3}|X[CL])(?:V?I{0,3}|I[XV])$/,
      token = /[MDLV]|C[MD]?|X[CL]?|I[XV]?/g,
      key = {
        M: 1000,
        CM: 900,
        D: 500,
        CD: 400,
        C: 100,
        XC: 90,
        L: 50,
        XL: 40,
        X: 10,
        IX: 9,
        V: 5,
        IV: 4,
        I: 1
      },
      num = 0,
      m;
    if (!(str && validator.test(str))) {
      return false;
    }
    while ((m = token.exec(str))) {
      num += key[m[0]];
    }
    return num;
  }



  function parsePageNumber(str) {
    var num = _.parseInt(str);
    if (_.isNaN(num)) {
      num = deromanize(str);
      if (!num) {
        console.error('Invalid PageNumber!!');
        return false;
      }
    } else {
      return num;
    }
  }

  function sortPages(pages) {
    _.forEach(pages, function (page) {
      page.pageNumber = parsePageNumber(page.PageNumber);
    });

    return pages.sort(function (pg0, pg1) {
      return pg0.pageNumber - pg1.pageNumber;
    });
  }

  // function hasNextPage() {
  //   if (getPageTags(_.sync.data.pageNumber + 1)) {
  //     return true;
  //   }
  //   return false;
  // }
  function goToPage(pageNumber) {
    var data = _.sync.data,
      tags = getPageTags(pageNumber);

    if (tags) {
      data.pageNumber = pageNumber;
      data.tags = tags;
      console.log('>>>previous el', data.el);
      data.el = _.getPageElement(pageNumber);
      console.log('>>>current el', data.el);
      if (!data.el) {
        console.error('Next page hasnt been loaded yet!!');
        return false;
      }
      return tags;
    }
    return false;
  }

  function goToNextPage() {
    var data = _.sync.data,
      pageNumber = data.pageNumber + 1,
      tags = goToPage(pageNumber);

    if (tags) {
      _.sync.data.i = 0;
    }
    return tags;
  }

  function isFirstPage() {
    var data = _.sync.data;
    return (data.pages[0].pageNumber === data.pageNumber);
  }

  function goToSeekedPage(desiredTime) {
    return _.some(_.sync.data.pages, function (page) {
      return _.some(page.Tags, function (tag, index) {
        var thisIsTheOne = false;
        if (page.Tags[index + 1]) {
          thisIsTheOne = (desiredTime > tag.SecondInMedia && desiredTime < page.Tags[index + 1].SecondInMedia);
        }
        // else {
        //   thisIsTheOne = (desiredTime > tag.SecondInMedia);
        // }
        if (thisIsTheOne) {
          if (goToPage(page.pageNumber)) {
            _.sync.data.i = index;
            return true;
          }
        }
      });
    });
  }

  function initData(data) {
    if (_.isObject(_.sync.data)) {
      _.sync.data.isFirstCall = true;
      _.sync.data.i = 0;
      _.sync.data.delta.ms = 0;
      _.sync.data.delta.lastCall = 0;
    } else {
      _.sync.data = {
        isFirstCall: true,
        i: 0,
        delta: {
          ms: 0,
          lastCall: 0
        }
      };
    }
    if (_.isObject(data)) {
      _.extend(_.sync.data, data);
    }
  }

  _.stopVideo = function () {
    unbindEvents();

    _.sync.pause();
    _.sync.ended();

    _.sync.current = {};
    _.sync.status = 'stopped';

    _.clear();
  };

  _.sync = function (node) {
    var filePath = $(node).attr('href'),
      mediaDom = $(node).closest('.media-dom'),
      bookId = mediaDom.attr('bookId'),
      pageNumber = mediaDom.attr('pageNumber'),
      mediaId = mediaDom.attr('mediaId'),
      documentId = mediaDom.attr('mediaId'),
      el;

    if (_.sync.status === 'running' && _.sync.current.bookId === bookId) {
      console.log('The syncing process has already started!');
      return;
    }

    _.sync.current.bookId = bookId;
    _.sync.status = 'running';

    bookId = _.parseInt(bookId, 10);
    pageNumber = _.parseInt(pageNumber, 10);
    mediaId = _.parseInt(mediaId, 10);
    documentId = _.getDocumentId(bookId, pageNumber);

    el = _.getPageElement(pageNumber);
    if (!el) {
      console.error('No page element!!');
      return;
    }
    _.ws.getTagsByMedia(mediaId, bookId)
      .then(function (pages) {

        pages = sortPages(pages);

        initData({
          el: el,
          pages: pages,
          bookId: bookId,
          mediaId: mediaId,
          pageNumber: pageNumber
        });

        var tags = getPageTags();
        _.sync.data.tags = tags;

        _.play(filePath)
          .then(function () {
            _.vid().pause();

            _.vid().currentTime = tags[0].SecondInMedia;
            _.$vid().on('seeked', function onseeked() {
              _.$vid().off('seeked', onseeked);

              bindEvents();
              _.vid().play();
            });
          });
      });
  };

  _.sync.current = {};

  _.sync.play = function () {
    var delta = _.sync.data.delta,
      isFirstCall = _.sync.data.isFirstCall;

    if (isFirstCall) {
      _.sync.data.isFirstCall = false;
      _.sync.playTags(true);
    } else {
      _.sync.tagsTimer = setTimeout(delta.lastFN, delta.timeLeft);
    }
    console.log('>>play');
  };

  _.sync.ended = function () {
    if (_.sync.data) {
      initData();
    } else {
      console.log('STOP playing!');
    }
  };

  _.sync.pause = function () {
    if (_.sync.tagsTimer) {

      _.sync.data.delta.timeLeft = _.sync.data.delta.ms - (new Date().getTime() - _.sync.data.delta.lastCallMoment);

      clearTimeout(_.sync.tagsTimer);

    }
    console.log('>>pause');
  };

  _.sync.seeking = function (e) {
    _.sync.seeking._lastCallWithArgs = arguments;

    console.log('>>seeking:', e);
  };

  _.sync.seeked = function (e) {

    goToSeekedPage(_.vid().currentTime);

    console.log('>>seeked:', e);
  };

  _.sync.playTags = function playTags(isFirst) {
    var data = _.sync.data,
      i = data.i,
      tags = data.tags,
      tag,
      start,
      // end,
      msDelta,
      doIterate = (i < tags.length),
      itIsANewPage = false;

    if (!doIterate) {
      tags = goToNextPage();
      if (tags) {
        doIterate = true;
        i = 0;
        itIsANewPage = true;
      }
    }

    if (doIterate) {
      tag = tags[i];

      // msDelta = (i === 0) ? tag.SecondInMedia : (tag.SecondInMedia - tags[i - 1].SecondInMedia);

      if (isFirst) {
        if (!isFirstPage()) {
          console.log('This is the very first page!');
        }
        // if (!isFirstPage()) {
        msDelta = 0;
        // } else {
        //   msDelta = tag.SecondInMedia;
        // }
      } else {
        if (itIsANewPage) {
          msDelta = tag.SecondInMedia - data.previousTag.SecondInMedia;
        } else {
          msDelta = (i === 0) ? tag.SecondInMedia : (tag.SecondInMedia - tags[i - 1].SecondInMedia);
        }
      }

      // msDelta = (i === 0) ? 0 : (tag.SecondInMedia - tags[i - 1].SecondInMedia);
      msDelta *= 1000;
      start = tag.PositionInDoc;
      //end = ((i === tags.length - 1) ? 10 : tags[i + 1].PositionInDoc);

      data.delta.ms = msDelta;
      data.delta.lastCallMoment = new Date().getTime();
      data.delta.lastFN = (function (start) {
        return function () {
          //TODO: it probably needs some review to check if we need to add the plus one(+1) to start
          _.highlightWord(_.sync.data.el, start);
          _.sync.data.i++;
          _.sync.playTags();
        };
      }(start));

      data.previousTag = tag;

      if (isFirst) {
        data.delta.lastFN();
      } else {
        _.sync.tagsTimer = setTimeout(data.delta.lastFN, msDelta);
      }

    } else {
      console.log('The End!');
      _.sync.ended(false);
    }
  };
}(window, jQuery));