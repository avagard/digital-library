(function (global, $) {

  var _ = global._;

  _.getInput = (function () {
    var currentCallback,
      titleWindow;

    function clickHandler() {
      var val = $.trim(titleWindow.down('textfield').getValue());
      if(!val){
        _.alert('لطفا اطلاعات لازم را وارد نمایید!');
        return;
      }

      if ($.isFunction(currentCallback)) {
        currentCallback(val);
      }
      
      titleWindow.hide();
      currentCallback = undefined;
    }

    titleWindow = Ext.create('Ext.window.Window', {
      title: 'نام',
      height: 55,
      width: 210,
      rtl: true,
      resizable: false,
      layout: 'hbox',
      items: [{
          xtype: 'textfield',
          name: 'text',
          width: 150
            //value: item.get('text')
        },
        Ext.create('Ext.Button', {
          text: 'تایید',
          width: 50,
          listeners: {
            click: clickHandler
          }
        })
      ]
    });

    global.titleWindow = titleWindow;

    return function (callback) {
      if ($.isFunction(callback)) {
        currentCallback = callback;
      }
      titleWindow.down('textfield').setValue('');
      titleWindow.show();
    };
  }());

}(this, jQuery));
