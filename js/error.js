
function error(app) {
  'use strict';

  /// catch 404 and forward to error handler
  app.use(function (req, res, next) {
    console.log('*****************************************************');
    console.log('*******************ERROR:Not Found*******************');
    console.log(req.url);
    console.log('*****************************************************');
    console.log('*****************************************************');

    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  /// error handlers

  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    app.use(function (err, req, res) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err
      });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use(function (err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });

}

module.exports = error;