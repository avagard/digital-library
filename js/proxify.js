var httpProxy = require('http-proxy');

function proxify(app) {
  'use strict';

  var apiProxy = httpProxy.createProxyServer();

  app.all('/WebServices*|/RTFHandler.ashx*|/MediaFiles*|/MediaService.svc*', function (req, res, next) {
    apiProxy.web(req, res, {
      target: 'http://94.232.172.91'
    }, function (e) {

      console.log('*************ProxyException********************');
      console.log('Url:' + req.url);
      console.error(e);
      console.log('*******************************************');

      next();

    });
  });



  /*apiProxy.on('proxyReq', function (proxyReq, req, res, options) {
    proxyReq.setHeader('X-Special-Proxy-Header', 'foobar');
  });*/
};

module.exports = proxify;
