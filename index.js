var express = require('express'),
  path = require('path'),
  logger = require('morgan'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  proxify = require('./js/proxify'),
  error = require('./js/error'),
  app = express();

proxify(app);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'src')));

error(app);

// app.all('*', function (req, res, next) {
//   res.set('Access-Control-Allow-Origin', '*');
//   res.set('Access-Control-Allow-Credentials', true);
//   res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
//   res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
//   if ('OPTIONS' === req.method) {
//     return res.send(200);
//   }
//   next();
// });

module.exports = app;